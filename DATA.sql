
INSERT INTO rights(right_id, right_desc)VALUES ( 1, 'Members');
INSERT INTO rights(right_id, right_desc)VALUES ( 2, 'Librarians');

insert into persons (increment_id, person_id, email, password, first_name, mid_name, last_name, capacity, period)
values (1,1,'42972667@qq.com','MTIzNDU2','Wangshui','','Kang',5,14);
insert into persons (increment_id, person_id, email, password, first_name, mid_name, last_name, capacity, period)
values (2,2,'admin@iit.edu','MTIzNDU2','administrator','','iit',5,14);
insert into persons (increment_id, person_id, email, password, first_name, mid_name, last_name, capacity, period)
values (3,3,'test@iit.edu','MTIzNDU2','test','','iit',5,14);

INSERT INTO has_r(right_id, person_id)VALUES ( 1, 1);
INSERT INTO has_r(right_id, person_id)VALUES ( 1, 2);
INSERT INTO has_r(right_id, person_id)VALUES ( 2, 2);
INSERT INTO has_r(right_id, person_id)VALUES ( 1, 3);
--Newspapers
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (1,1,'Books' ,'ISBN');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (2,2,'Journal Articles' ,'ISSN');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (3,3,'Magazines' ,'ISSN');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (4,4,'Recordings' ,'ISRC');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (5,5,'Scores' ,'ISMN');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (6,6,'Musical Works' ,'ISWC');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (7,7,'Audio-Visual Materials' ,'ISAN');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (8,8,'Text Word' ,'ISTC');

INSERT INTO category(increment_id, category_id, category_name)	VALUES (1,1,'Arts & Photography');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (2,2,'Biographies & Memoirs');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (3,3,'Business & Money');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (4,4,'Children''s eBooks');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (5,5,'Comics, Manga & Graphic Novels');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (6,6,'Computers & Technology');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (7,7,'Cookbooks, Food & Wine');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (8,8,'Crafts, Hobbies & Home');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (9,9,'Education & Teaching');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (10,10,'Engineering & Transportation');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (11,11,'Foreign Languages');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (12,12,'Health, Fitness & Dieting');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (13,13,'History');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (14,14,'Humor & Entertainment');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (15,15,'Law');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (16,16,'LGBTQ+ eBooks');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (17,17,'Literature & Fiction');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (18,18,'Medical eBooks');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (19,19,'Mystery, Thriller & Suspense');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (20,20,'Nonfiction');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (21,21,'Parenting & Relationships');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (22,22,'Politics & Social Sciences');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (23,23,'Reference');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (24,24,'Religion & Spirituality');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (25,25,'Romance');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (26,26,'Science & Math');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (27,27,'Science Fiction & Fantasy');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (28,28,'Self-Help');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (29,29,'Sports & Outdoors');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (30,30,'Teen & Young Adult');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (31,31,'Travel');

INSERT INTO has_d(doc_document_id,doc_type_att_id,  doc_type_att_value)	VALUES (2, 1, 'Title1');
INSERT INTO has_d(doc_document_id,doc_type_att_id,  doc_type_att_value)	VALUES (2, 2, 'Name1');
INSERT INTO has_d(doc_document_id,doc_type_att_id,  doc_type_att_value)	VALUES (2, 3, 'Ketword1');

/*
Demo Video Data：
Category
32	"CategoryChildVideo"

doc_type
9	"ChildVideo"	"ISRC"

doc_type_attributes
4	"ChildAge"	4

documents
Snow White and the Seven Dwarfs
Grimm's Fairy Tales
Snow White,the Seven Dwarfs,Grimm's Fairy Tales
3-5

SET search_path to "Group1";
delete from borrow where doc_copy_id=10;
delete from documents where document_id=10;
delete from has_d where doc_document_id =10;
delete from doc_type_attributes where doc_type_att_id=4;
delete from doc_types where doc_type_id=9;
delete from Category where category_id=32;

















guest:
home
search\search a word\borrow\detail
login
person\regist\update\BorrowReturn
system\persons\document\category\doctypes
collapse\expand

user with member rights
login\42972667@qq.com
search\input bi to search\borrow
person\update\BorrowReturn
system\persons\document\category\doctypes
logout

user with member and admin rights
login\admin@iit.edu
system\persons\document\category\doctypes
category\add CategoryChildVideo 32
doc_type\add "ChildVideo"	"ISRC"	9
doc_type\add "ChildAge"	4
documents\Snow White and the Seven Dwarfs | Grimm's Fairy Tales | Snow White,the Seven Dwarfs,Grimm's Fairy Tales |3-5
search\borrow
person\BorrowReturn
*/
















