package com.dvo.exception;

import com.dvo.common.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常捕获
 */
@Slf4j
@ControllerAdvice
public class GlobalDefaultExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ApiResult handleRuntimeException(Exception e) {
        if(e instanceof RuntimeException) {
            return ApiResult.success(e.getMessage());
        }
        log.error(e.getMessage());
        return ApiResult.error("未知错误");
    }


}
