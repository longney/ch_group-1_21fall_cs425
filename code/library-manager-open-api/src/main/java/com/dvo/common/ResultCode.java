package com.dvo.common;

public interface ResultCode {

  String SUCCESS = "success";

  String ERROR = "error";

  String VALID = "valid";

  String INVALID = "invalid";

  Integer ERR_NO_TOKEN = 10001;

  Integer ERR_TOKEN_EXPIRE = 10002;

}
