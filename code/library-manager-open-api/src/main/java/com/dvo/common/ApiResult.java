package com.dvo.common;

import com.google.common.collect.Maps;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;

/**
 * 统一返回对象
 * @param <T>
 */
@Setter
public class ApiResult<T> {
  public String remark;
  public T data;
  public String message;
  public Integer code;

  public ApiResult(String result) {
    this.remark = result;
    this.code = HttpStatus.BAD_REQUEST.value();
    this.message = "";
  }

  public ApiResult(String result, T content) {
    this.remark = result;
    this.code = HttpStatus.OK.value();
    this.data = content;
    this.message = "";
  }

  public ApiResult(String result, String message) {
    this.remark = result;
    this.code = HttpStatus.BAD_REQUEST.value();
    this.message = message;
  }

  public ApiResult(String result, String message, Integer code) {
    this.remark = result;
    this.code = code;
    this.message = message;
  }

  public static ApiResult success(Object context) {
    return new ApiResult(ResultCode.SUCCESS, context);
  }

  public static ApiResult success() {
    return success(Maps.newHashMap());
  }

  public static ApiResult error() {
    return new ApiResult(ResultCode.ERROR);
  }

  public static ApiResult error(String message) {
    return new ApiResult(ResultCode.ERROR, message);
  }

  public static ApiResult error(String message, Integer code) {
    return new ApiResult(ResultCode.ERROR, message, code);
  }

  public static ApiResult errors(List<String> messages) {
    return new ApiResult(ResultCode.ERROR, messages);
  }
}
