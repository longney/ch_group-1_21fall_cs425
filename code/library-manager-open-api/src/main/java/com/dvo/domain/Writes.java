package com.dvo.domain;

import java.io.Serializable;

public class Writes implements Serializable {
    private Integer incrementId;

    private Integer docContId;

    private Integer docDocContId;

    private Integer personId;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocContId() {
        return docContId;
    }

    public void setDocContId(Integer docContId) {
        this.docContId = docContId;
    }

    public Integer getDocDocContId() {
        return docDocContId;
    }

    public void setDocDocContId(Integer docDocContId) {
        this.docDocContId = docDocContId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Writes other = (Writes) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocContId() == null ? other.getDocContId() == null : this.getDocContId().equals(other.getDocContId()))
            && (this.getDocDocContId() == null ? other.getDocDocContId() == null : this.getDocDocContId().equals(other.getDocDocContId()))
            && (this.getPersonId() == null ? other.getPersonId() == null : this.getPersonId().equals(other.getPersonId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocContId() == null) ? 0 : getDocContId().hashCode());
        result = prime * result + ((getDocDocContId() == null) ? 0 : getDocDocContId().hashCode());
        result = prime * result + ((getPersonId() == null) ? 0 : getPersonId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", docContId=").append(docContId);
        sb.append(", docDocContId=").append(docDocContId);
        sb.append(", personId=").append(personId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}