package com.dvo.domain;

import java.io.Serializable;

public class DocContents implements Serializable {
    private Integer incrementId;

    private Integer docContId;

    private String docContTitle;

    private String docContKeywords;

    private Integer docDocContId;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocContId() {
        return docContId;
    }

    public void setDocContId(Integer docContId) {
        this.docContId = docContId;
    }

    public String getDocContTitle() {
        return docContTitle;
    }

    public void setDocContTitle(String docContTitle) {
        this.docContTitle = docContTitle == null ? null : docContTitle.trim();
    }

    public String getDocContKeywords() {
        return docContKeywords;
    }

    public void setDocContKeywords(String docContKeywords) {
        this.docContKeywords = docContKeywords == null ? null : docContKeywords.trim();
    }

    public Integer getDocDocContId() {
        return docDocContId;
    }

    public void setDocDocContId(Integer docDocContId) {
        this.docDocContId = docDocContId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DocContents other = (DocContents) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocContId() == null ? other.getDocContId() == null : this.getDocContId().equals(other.getDocContId()))
            && (this.getDocContTitle() == null ? other.getDocContTitle() == null : this.getDocContTitle().equals(other.getDocContTitle()))
            && (this.getDocContKeywords() == null ? other.getDocContKeywords() == null : this.getDocContKeywords().equals(other.getDocContKeywords()))
            && (this.getDocDocContId() == null ? other.getDocDocContId() == null : this.getDocDocContId().equals(other.getDocDocContId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocContId() == null) ? 0 : getDocContId().hashCode());
        result = prime * result + ((getDocContTitle() == null) ? 0 : getDocContTitle().hashCode());
        result = prime * result + ((getDocContKeywords() == null) ? 0 : getDocContKeywords().hashCode());
        result = prime * result + ((getDocDocContId() == null) ? 0 : getDocDocContId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", docContId=").append(docContId);
        sb.append(", docContTitle=").append(docContTitle);
        sb.append(", docContKeywords=").append(docContKeywords);
        sb.append(", docDocContId=").append(docDocContId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}