package com.dvo.domain;

import java.io.Serializable;

public class DocTypeAttributes implements Serializable {
    private Integer incrementId;

    private Integer docTypeAttId;

    private String docTypeAttDesc;

    private Integer displayOrder;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocTypeAttId() {
        return docTypeAttId;
    }

    public void setDocTypeAttId(Integer docTypeAttId) {
        this.docTypeAttId = docTypeAttId;
    }

    public String getDocTypeAttDesc() {
        return docTypeAttDesc;
    }

    public void setDocTypeAttDesc(String docTypeAttDesc) {
        this.docTypeAttDesc = docTypeAttDesc == null ? null : docTypeAttDesc.trim();
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DocTypeAttributes other = (DocTypeAttributes) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocTypeAttId() == null ? other.getDocTypeAttId() == null : this.getDocTypeAttId().equals(other.getDocTypeAttId()))
            && (this.getDocTypeAttDesc() == null ? other.getDocTypeAttDesc() == null : this.getDocTypeAttDesc().equals(other.getDocTypeAttDesc()))
            && (this.getDisplayOrder() == null ? other.getDisplayOrder() == null : this.getDisplayOrder().equals(other.getDisplayOrder()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocTypeAttId() == null) ? 0 : getDocTypeAttId().hashCode());
        result = prime * result + ((getDocTypeAttDesc() == null) ? 0 : getDocTypeAttDesc().hashCode());
        result = prime * result + ((getDisplayOrder() == null) ? 0 : getDisplayOrder().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", docTypeAttId=").append(docTypeAttId);
        sb.append(", docTypeAttDesc=").append(docTypeAttDesc);
        sb.append(", displayOrder=").append(displayOrder);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}