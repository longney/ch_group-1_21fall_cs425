package com.dvo.domain;

import java.io.Serializable;
import java.util.Date;

public class Publish implements Serializable {
    private Integer incrementId;

    private Integer documentId;

    private Integer docVerId;

    private Integer publisherId;

    private Integer publishCode;

    private Date publishDate;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Integer documentId) {
        this.documentId = documentId;
    }

    public Integer getDocVerId() {
        return docVerId;
    }

    public void setDocVerId(Integer docVerId) {
        this.docVerId = docVerId;
    }

    public Integer getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    public Integer getPublishCode() {
        return publishCode;
    }

    public void setPublishCode(Integer publishCode) {
        this.publishCode = publishCode;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Publish other = (Publish) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocumentId() == null ? other.getDocumentId() == null : this.getDocumentId().equals(other.getDocumentId()))
            && (this.getDocVerId() == null ? other.getDocVerId() == null : this.getDocVerId().equals(other.getDocVerId()))
            && (this.getPublisherId() == null ? other.getPublisherId() == null : this.getPublisherId().equals(other.getPublisherId()))
            && (this.getPublishCode() == null ? other.getPublishCode() == null : this.getPublishCode().equals(other.getPublishCode()))
            && (this.getPublishDate() == null ? other.getPublishDate() == null : this.getPublishDate().equals(other.getPublishDate()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocumentId() == null) ? 0 : getDocumentId().hashCode());
        result = prime * result + ((getDocVerId() == null) ? 0 : getDocVerId().hashCode());
        result = prime * result + ((getPublisherId() == null) ? 0 : getPublisherId().hashCode());
        result = prime * result + ((getPublishCode() == null) ? 0 : getPublishCode().hashCode());
        result = prime * result + ((getPublishDate() == null) ? 0 : getPublishDate().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", documentId=").append(documentId);
        sb.append(", docVerId=").append(docVerId);
        sb.append(", publisherId=").append(publisherId);
        sb.append(", publishCode=").append(publishCode);
        sb.append(", publishDate=").append(publishDate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}