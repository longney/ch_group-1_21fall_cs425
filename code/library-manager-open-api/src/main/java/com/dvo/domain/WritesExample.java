package com.dvo.domain;

import java.util.ArrayList;
import java.util.List;

public class WritesExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WritesExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIncrementIdIsNull() {
            addCriterion("increment_id is null");
            return (Criteria) this;
        }

        public Criteria andIncrementIdIsNotNull() {
            addCriterion("increment_id is not null");
            return (Criteria) this;
        }

        public Criteria andIncrementIdEqualTo(Integer value) {
            addCriterion("increment_id =", value, "incrementId");
            return (Criteria) this;
        }

        public Criteria andIncrementIdNotEqualTo(Integer value) {
            addCriterion("increment_id <>", value, "incrementId");
            return (Criteria) this;
        }

        public Criteria andIncrementIdGreaterThan(Integer value) {
            addCriterion("increment_id >", value, "incrementId");
            return (Criteria) this;
        }

        public Criteria andIncrementIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("increment_id >=", value, "incrementId");
            return (Criteria) this;
        }

        public Criteria andIncrementIdLessThan(Integer value) {
            addCriterion("increment_id <", value, "incrementId");
            return (Criteria) this;
        }

        public Criteria andIncrementIdLessThanOrEqualTo(Integer value) {
            addCriterion("increment_id <=", value, "incrementId");
            return (Criteria) this;
        }

        public Criteria andIncrementIdIn(List<Integer> values) {
            addCriterion("increment_id in", values, "incrementId");
            return (Criteria) this;
        }

        public Criteria andIncrementIdNotIn(List<Integer> values) {
            addCriterion("increment_id not in", values, "incrementId");
            return (Criteria) this;
        }

        public Criteria andIncrementIdBetween(Integer value1, Integer value2) {
            addCriterion("increment_id between", value1, value2, "incrementId");
            return (Criteria) this;
        }

        public Criteria andIncrementIdNotBetween(Integer value1, Integer value2) {
            addCriterion("increment_id not between", value1, value2, "incrementId");
            return (Criteria) this;
        }

        public Criteria andDocContIdIsNull() {
            addCriterion("doc_cont_id is null");
            return (Criteria) this;
        }

        public Criteria andDocContIdIsNotNull() {
            addCriterion("doc_cont_id is not null");
            return (Criteria) this;
        }

        public Criteria andDocContIdEqualTo(Integer value) {
            addCriterion("doc_cont_id =", value, "docContId");
            return (Criteria) this;
        }

        public Criteria andDocContIdNotEqualTo(Integer value) {
            addCriterion("doc_cont_id <>", value, "docContId");
            return (Criteria) this;
        }

        public Criteria andDocContIdGreaterThan(Integer value) {
            addCriterion("doc_cont_id >", value, "docContId");
            return (Criteria) this;
        }

        public Criteria andDocContIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("doc_cont_id >=", value, "docContId");
            return (Criteria) this;
        }

        public Criteria andDocContIdLessThan(Integer value) {
            addCriterion("doc_cont_id <", value, "docContId");
            return (Criteria) this;
        }

        public Criteria andDocContIdLessThanOrEqualTo(Integer value) {
            addCriterion("doc_cont_id <=", value, "docContId");
            return (Criteria) this;
        }

        public Criteria andDocContIdIn(List<Integer> values) {
            addCriterion("doc_cont_id in", values, "docContId");
            return (Criteria) this;
        }

        public Criteria andDocContIdNotIn(List<Integer> values) {
            addCriterion("doc_cont_id not in", values, "docContId");
            return (Criteria) this;
        }

        public Criteria andDocContIdBetween(Integer value1, Integer value2) {
            addCriterion("doc_cont_id between", value1, value2, "docContId");
            return (Criteria) this;
        }

        public Criteria andDocContIdNotBetween(Integer value1, Integer value2) {
            addCriterion("doc_cont_id not between", value1, value2, "docContId");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdIsNull() {
            addCriterion("doc_doc_cont_id is null");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdIsNotNull() {
            addCriterion("doc_doc_cont_id is not null");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdEqualTo(Integer value) {
            addCriterion("doc_doc_cont_id =", value, "docDocContId");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdNotEqualTo(Integer value) {
            addCriterion("doc_doc_cont_id <>", value, "docDocContId");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdGreaterThan(Integer value) {
            addCriterion("doc_doc_cont_id >", value, "docDocContId");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("doc_doc_cont_id >=", value, "docDocContId");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdLessThan(Integer value) {
            addCriterion("doc_doc_cont_id <", value, "docDocContId");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdLessThanOrEqualTo(Integer value) {
            addCriterion("doc_doc_cont_id <=", value, "docDocContId");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdIn(List<Integer> values) {
            addCriterion("doc_doc_cont_id in", values, "docDocContId");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdNotIn(List<Integer> values) {
            addCriterion("doc_doc_cont_id not in", values, "docDocContId");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdBetween(Integer value1, Integer value2) {
            addCriterion("doc_doc_cont_id between", value1, value2, "docDocContId");
            return (Criteria) this;
        }

        public Criteria andDocDocContIdNotBetween(Integer value1, Integer value2) {
            addCriterion("doc_doc_cont_id not between", value1, value2, "docDocContId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("person_id is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("person_id is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(Integer value) {
            addCriterion("person_id =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(Integer value) {
            addCriterion("person_id <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(Integer value) {
            addCriterion("person_id >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("person_id >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(Integer value) {
            addCriterion("person_id <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(Integer value) {
            addCriterion("person_id <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<Integer> values) {
            addCriterion("person_id in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<Integer> values) {
            addCriterion("person_id not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(Integer value1, Integer value2) {
            addCriterion("person_id between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(Integer value1, Integer value2) {
            addCriterion("person_id not between", value1, value2, "personId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}