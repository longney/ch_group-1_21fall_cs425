package com.dvo.domain;

import java.io.Serializable;

public class DocTypes implements Serializable {
    private Integer incrementId;

    private Integer docTypeId;

    private String docTypeName;

    private String docTypeCode;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeId(Integer docTypeId) {
        this.docTypeId = docTypeId;
    }

    public String getDocTypeName() {
        return docTypeName;
    }

    public void setDocTypeName(String docTypeName) {
        this.docTypeName = docTypeName == null ? null : docTypeName.trim();
    }

    public String getDocTypeCode() {
        return docTypeCode;
    }

    public void setDocTypeCode(String docTypeCode) {
        this.docTypeCode = docTypeCode == null ? null : docTypeCode.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DocTypes other = (DocTypes) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocTypeId() == null ? other.getDocTypeId() == null : this.getDocTypeId().equals(other.getDocTypeId()))
            && (this.getDocTypeName() == null ? other.getDocTypeName() == null : this.getDocTypeName().equals(other.getDocTypeName()))
            && (this.getDocTypeCode() == null ? other.getDocTypeCode() == null : this.getDocTypeCode().equals(other.getDocTypeCode()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocTypeId() == null) ? 0 : getDocTypeId().hashCode());
        result = prime * result + ((getDocTypeName() == null) ? 0 : getDocTypeName().hashCode());
        result = prime * result + ((getDocTypeCode() == null) ? 0 : getDocTypeCode().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", docTypeId=").append(docTypeId);
        sb.append(", docTypeName=").append(docTypeName);
        sb.append(", docTypeCode=").append(docTypeCode);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}