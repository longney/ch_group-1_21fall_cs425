package com.dvo.domain;

import java.io.Serializable;

public class HasD implements Serializable {
    private Integer incrementId;

    private Integer docTypeAttId;

    private Integer docDocumentId;

    private String docTypeAttValue;

    public void setDocDocumentId(Integer docDocumentId) {
        this.docDocumentId = docDocumentId;
    }

    public Integer getDocDocumentId() {
        return docDocumentId;
    }

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocTypeAttId() {
        return docTypeAttId;
    }

    public void setDocTypeAttId(Integer docTypeAttId) {
        this.docTypeAttId = docTypeAttId;
    }



    public String getDocTypeAttValue() {
        return docTypeAttValue;
    }

    public void setDocTypeAttValue(String docTypeAttValue) {
        this.docTypeAttValue = docTypeAttValue == null ? null : docTypeAttValue.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        HasD other = (HasD) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocTypeAttId() == null ? other.getDocTypeAttId() == null : this.getDocTypeAttId().equals(other.getDocTypeAttId()))
            && (this.getDocDocumentId() == null ? other.getDocDocumentId() == null : this.getDocDocumentId().equals(other.getDocDocumentId()))
            && (this.getDocTypeAttValue() == null ? other.getDocTypeAttValue() == null : this.getDocTypeAttValue().equals(other.getDocTypeAttValue()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocTypeAttId() == null) ? 0 : getDocTypeAttId().hashCode());
        result = prime * result + ((getDocDocumentId() == null) ? 0 : getDocDocumentId().hashCode());
        result = prime * result + ((getDocTypeAttValue() == null) ? 0 : getDocTypeAttValue().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", docTypeAttId=").append(docTypeAttId);
        sb.append(", docDocumentId=").append(docDocumentId);
        sb.append(", docTypeAttValue=").append(docTypeAttValue);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}