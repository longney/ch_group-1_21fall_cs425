package com.dvo.domain;

import java.io.Serializable;
import java.util.Date;

public class SearchContent implements Serializable {
    private Integer incrementId;

    private Integer searchContentId;

    private String sPublisher;

    private String sDootype;

    private String sCategory;

    private String sDooname;

    private String sDockeyword;

    private String sDocconttitle;

    private String sDoccontkeyword;

    private String sAuthor;

    private Integer sPublishcode;

    private Date sPublishdate;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getSearchContentId() {
        return searchContentId;
    }

    public void setSearchContentId(Integer searchContentId) {
        this.searchContentId = searchContentId;
    }

    public String getsPublisher() {
        return sPublisher;
    }

    public void setsPublisher(String sPublisher) {
        this.sPublisher = sPublisher == null ? null : sPublisher.trim();
    }

    public String getsDootype() {
        return sDootype;
    }

    public void setsDootype(String sDootype) {
        this.sDootype = sDootype == null ? null : sDootype.trim();
    }

    public String getsCategory() {
        return sCategory;
    }

    public void setsCategory(String sCategory) {
        this.sCategory = sCategory == null ? null : sCategory.trim();
    }

    public String getsDooname() {
        return sDooname;
    }

    public void setsDooname(String sDooname) {
        this.sDooname = sDooname == null ? null : sDooname.trim();
    }

    public String getsDockeyword() {
        return sDockeyword;
    }

    public void setsDockeyword(String sDockeyword) {
        this.sDockeyword = sDockeyword == null ? null : sDockeyword.trim();
    }

    public String getsDocconttitle() {
        return sDocconttitle;
    }

    public void setsDocconttitle(String sDocconttitle) {
        this.sDocconttitle = sDocconttitle == null ? null : sDocconttitle.trim();
    }

    public String getsDoccontkeyword() {
        return sDoccontkeyword;
    }

    public void setsDoccontkeyword(String sDoccontkeyword) {
        this.sDoccontkeyword = sDoccontkeyword == null ? null : sDoccontkeyword.trim();
    }

    public String getsAuthor() {
        return sAuthor;
    }

    public void setsAuthor(String sAuthor) {
        this.sAuthor = sAuthor == null ? null : sAuthor.trim();
    }

    public Integer getsPublishcode() {
        return sPublishcode;
    }

    public void setsPublishcode(Integer sPublishcode) {
        this.sPublishcode = sPublishcode;
    }

    public Date getsPublishdate() {
        return sPublishdate;
    }

    public void setsPublishdate(Date sPublishdate) {
        this.sPublishdate = sPublishdate;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SearchContent other = (SearchContent) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getSearchContentId() == null ? other.getSearchContentId() == null : this.getSearchContentId().equals(other.getSearchContentId()))
            && (this.getsPublisher() == null ? other.getsPublisher() == null : this.getsPublisher().equals(other.getsPublisher()))
            && (this.getsDootype() == null ? other.getsDootype() == null : this.getsDootype().equals(other.getsDootype()))
            && (this.getsCategory() == null ? other.getsCategory() == null : this.getsCategory().equals(other.getsCategory()))
            && (this.getsDooname() == null ? other.getsDooname() == null : this.getsDooname().equals(other.getsDooname()))
            && (this.getsDockeyword() == null ? other.getsDockeyword() == null : this.getsDockeyword().equals(other.getsDockeyword()))
            && (this.getsDocconttitle() == null ? other.getsDocconttitle() == null : this.getsDocconttitle().equals(other.getsDocconttitle()))
            && (this.getsDoccontkeyword() == null ? other.getsDoccontkeyword() == null : this.getsDoccontkeyword().equals(other.getsDoccontkeyword()))
            && (this.getsAuthor() == null ? other.getsAuthor() == null : this.getsAuthor().equals(other.getsAuthor()))
            && (this.getsPublishcode() == null ? other.getsPublishcode() == null : this.getsPublishcode().equals(other.getsPublishcode()))
            && (this.getsPublishdate() == null ? other.getsPublishdate() == null : this.getsPublishdate().equals(other.getsPublishdate()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getSearchContentId() == null) ? 0 : getSearchContentId().hashCode());
        result = prime * result + ((getsPublisher() == null) ? 0 : getsPublisher().hashCode());
        result = prime * result + ((getsDootype() == null) ? 0 : getsDootype().hashCode());
        result = prime * result + ((getsCategory() == null) ? 0 : getsCategory().hashCode());
        result = prime * result + ((getsDooname() == null) ? 0 : getsDooname().hashCode());
        result = prime * result + ((getsDockeyword() == null) ? 0 : getsDockeyword().hashCode());
        result = prime * result + ((getsDocconttitle() == null) ? 0 : getsDocconttitle().hashCode());
        result = prime * result + ((getsDoccontkeyword() == null) ? 0 : getsDoccontkeyword().hashCode());
        result = prime * result + ((getsAuthor() == null) ? 0 : getsAuthor().hashCode());
        result = prime * result + ((getsPublishcode() == null) ? 0 : getsPublishcode().hashCode());
        result = prime * result + ((getsPublishdate() == null) ? 0 : getsPublishdate().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", searchContentId=").append(searchContentId);
        sb.append(", sPublisher=").append(sPublisher);
        sb.append(", sDootype=").append(sDootype);
        sb.append(", sCategory=").append(sCategory);
        sb.append(", sDooname=").append(sDooname);
        sb.append(", sDockeyword=").append(sDockeyword);
        sb.append(", sDocconttitle=").append(sDocconttitle);
        sb.append(", sDoccontkeyword=").append(sDoccontkeyword);
        sb.append(", sAuthor=").append(sAuthor);
        sb.append(", sPublishcode=").append(sPublishcode);
        sb.append(", sPublishdate=").append(sPublishdate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}