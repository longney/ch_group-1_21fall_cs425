package com.dvo.domain;

import java.io.Serializable;

public class Reference implements Serializable {
    private Integer incrementId;

    private Integer docDocContId;

    private Integer docDocDocContId2;

    private Integer docContId;

    private Integer docDocDocContId;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocDocContId() {
        return docDocContId;
    }

    public void setDocDocContId(Integer docDocContId) {
        this.docDocContId = docDocContId;
    }

    public Integer getDocDocDocContId2() {
        return docDocDocContId2;
    }

    public void setDocDocDocContId2(Integer docDocDocContId2) {
        this.docDocDocContId2 = docDocDocContId2;
    }

    public Integer getDocContId() {
        return docContId;
    }

    public void setDocContId(Integer docContId) {
        this.docContId = docContId;
    }

    public Integer getDocDocDocContId() {
        return docDocDocContId;
    }

    public void setDocDocDocContId(Integer docDocDocContId) {
        this.docDocDocContId = docDocDocContId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Reference other = (Reference) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocDocContId() == null ? other.getDocDocContId() == null : this.getDocDocContId().equals(other.getDocDocContId()))
            && (this.getDocDocDocContId2() == null ? other.getDocDocDocContId2() == null : this.getDocDocDocContId2().equals(other.getDocDocDocContId2()))
            && (this.getDocContId() == null ? other.getDocContId() == null : this.getDocContId().equals(other.getDocContId()))
            && (this.getDocDocDocContId() == null ? other.getDocDocDocContId() == null : this.getDocDocDocContId().equals(other.getDocDocDocContId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocDocContId() == null) ? 0 : getDocDocContId().hashCode());
        result = prime * result + ((getDocDocDocContId2() == null) ? 0 : getDocDocDocContId2().hashCode());
        result = prime * result + ((getDocContId() == null) ? 0 : getDocContId().hashCode());
        result = prime * result + ((getDocDocDocContId() == null) ? 0 : getDocDocDocContId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", docDocContId=").append(docDocContId);
        sb.append(", docDocDocContId2=").append(docDocDocContId2);
        sb.append(", docContId=").append(docContId);
        sb.append(", docDocDocContId=").append(docDocDocContId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}