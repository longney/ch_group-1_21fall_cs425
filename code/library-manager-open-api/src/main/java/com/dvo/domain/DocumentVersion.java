package com.dvo.domain;

import java.io.Serializable;

public class DocumentVersion implements Serializable {
    private Integer incrementId;

    private Integer docVerId;

    private Integer docContId;

    private Integer docDocContId;

    private String verDesc;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocVerId() {
        return docVerId;
    }

    public void setDocVerId(Integer docVerId) {
        this.docVerId = docVerId;
    }

    public Integer getDocContId() {
        return docContId;
    }

    public void setDocContId(Integer docContId) {
        this.docContId = docContId;
    }

    public Integer getDocDocContId() {
        return docDocContId;
    }

    public void setDocDocContId(Integer docDocContId) {
        this.docDocContId = docDocContId;
    }

    public String getVerDesc() {
        return verDesc;
    }

    public void setVerDesc(String verDesc) {
        this.verDesc = verDesc == null ? null : verDesc.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DocumentVersion other = (DocumentVersion) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocVerId() == null ? other.getDocVerId() == null : this.getDocVerId().equals(other.getDocVerId()))
            && (this.getDocContId() == null ? other.getDocContId() == null : this.getDocContId().equals(other.getDocContId()))
            && (this.getDocDocContId() == null ? other.getDocDocContId() == null : this.getDocDocContId().equals(other.getDocDocContId()))
            && (this.getVerDesc() == null ? other.getVerDesc() == null : this.getVerDesc().equals(other.getVerDesc()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocVerId() == null) ? 0 : getDocVerId().hashCode());
        result = prime * result + ((getDocContId() == null) ? 0 : getDocContId().hashCode());
        result = prime * result + ((getDocDocContId() == null) ? 0 : getDocDocContId().hashCode());
        result = prime * result + ((getVerDesc() == null) ? 0 : getVerDesc().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", docVerId=").append(docVerId);
        sb.append(", docContId=").append(docContId);
        sb.append(", docDocContId=").append(docDocContId);
        sb.append(", verDesc=").append(verDesc);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}