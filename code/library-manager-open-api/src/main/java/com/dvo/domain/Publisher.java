package com.dvo.domain;

import java.io.Serializable;

public class Publisher implements Serializable {
    private Integer incrementId;

    private Integer publisherId;

    private String publisherName;

    private String publisherAddr;

    private String publisherEmail;

    private String publisherPost;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName == null ? null : publisherName.trim();
    }

    public String getPublisherAddr() {
        return publisherAddr;
    }

    public void setPublisherAddr(String publisherAddr) {
        this.publisherAddr = publisherAddr == null ? null : publisherAddr.trim();
    }

    public String getPublisherEmail() {
        return publisherEmail;
    }

    public void setPublisherEmail(String publisherEmail) {
        this.publisherEmail = publisherEmail == null ? null : publisherEmail.trim();
    }

    public String getPublisherPost() {
        return publisherPost;
    }

    public void setPublisherPost(String publisherPost) {
        this.publisherPost = publisherPost == null ? null : publisherPost.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Publisher other = (Publisher) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getPublisherId() == null ? other.getPublisherId() == null : this.getPublisherId().equals(other.getPublisherId()))
            && (this.getPublisherName() == null ? other.getPublisherName() == null : this.getPublisherName().equals(other.getPublisherName()))
            && (this.getPublisherAddr() == null ? other.getPublisherAddr() == null : this.getPublisherAddr().equals(other.getPublisherAddr()))
            && (this.getPublisherEmail() == null ? other.getPublisherEmail() == null : this.getPublisherEmail().equals(other.getPublisherEmail()))
            && (this.getPublisherPost() == null ? other.getPublisherPost() == null : this.getPublisherPost().equals(other.getPublisherPost()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getPublisherId() == null) ? 0 : getPublisherId().hashCode());
        result = prime * result + ((getPublisherName() == null) ? 0 : getPublisherName().hashCode());
        result = prime * result + ((getPublisherAddr() == null) ? 0 : getPublisherAddr().hashCode());
        result = prime * result + ((getPublisherEmail() == null) ? 0 : getPublisherEmail().hashCode());
        result = prime * result + ((getPublisherPost() == null) ? 0 : getPublisherPost().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", publisherId=").append(publisherId);
        sb.append(", publisherName=").append(publisherName);
        sb.append(", publisherAddr=").append(publisherAddr);
        sb.append(", publisherEmail=").append(publisherEmail);
        sb.append(", publisherPost=").append(publisherPost);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}