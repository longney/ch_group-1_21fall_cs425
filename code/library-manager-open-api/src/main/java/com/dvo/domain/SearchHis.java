package com.dvo.domain;

import java.io.Serializable;

public class SearchHis implements Serializable {
    private Integer incrementId;

    private Integer personId;

    private Integer searchContentId;

    private String sType;

    private String sValue;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getSearchContentId() {
        return searchContentId;
    }

    public void setSearchContentId(Integer searchContentId) {
        this.searchContentId = searchContentId;
    }

    public String getsType() {
        return sType;
    }

    public void setsType(String sType) {
        this.sType = sType == null ? null : sType.trim();
    }

    public String getsValue() {
        return sValue;
    }

    public void setsValue(String sValue) {
        this.sValue = sValue == null ? null : sValue.trim();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SearchHis other = (SearchHis) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getPersonId() == null ? other.getPersonId() == null : this.getPersonId().equals(other.getPersonId()))
            && (this.getSearchContentId() == null ? other.getSearchContentId() == null : this.getSearchContentId().equals(other.getSearchContentId()))
            && (this.getsType() == null ? other.getsType() == null : this.getsType().equals(other.getsType()))
            && (this.getsValue() == null ? other.getsValue() == null : this.getsValue().equals(other.getsValue()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getPersonId() == null) ? 0 : getPersonId().hashCode());
        result = prime * result + ((getSearchContentId() == null) ? 0 : getSearchContentId().hashCode());
        result = prime * result + ((getsType() == null) ? 0 : getsType().hashCode());
        result = prime * result + ((getsValue() == null) ? 0 : getsValue().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", personId=").append(personId);
        sb.append(", searchContentId=").append(searchContentId);
        sb.append(", sType=").append(sType);
        sb.append(", sValue=").append(sValue);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}