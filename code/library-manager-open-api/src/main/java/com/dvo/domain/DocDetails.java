package com.dvo.domain;

import java.io.Serializable;
import java.util.Date;

public class DocDetails implements Serializable {
    private Integer incrementId;

    private Integer documentId;

    private Integer categoryId;

    private Integer docTypeId;

    private static final long serialVersionUID = 1L;

    private String categoryName;
    private String docTypeName;
    private String docTypeCode;

    private Integer docTypeAttId;
    private String docTypeAttDesc;
    private Integer displayOrder;

    private String docTypeAttValue;


    private String publisherName;
    private String publisherAddr;
    private String publisherEmail;
    private String publisherPost;

    private Integer publishCode;
    private Date publishDate;

    private String doctitle;
    private String docname;
    private String dockeyword;

    public String getDoctitle() {
        return doctitle;
    }

    public String getDocname() {
        return docname;
    }

    public String getDockeyword() {
        return dockeyword;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public String getPublisherAddr() {
        return publisherAddr;
    }

    public String getPublisherEmail() {
        return publisherEmail;
    }

    public String getPublisherPost() {
        return publisherPost;
    }

    public Integer getPublishCode() {
        return publishCode;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getDocTypeName() {
        return docTypeName;
    }

    public String getDocTypeCode() {
        return docTypeCode;
    }

    public Integer getDocTypeAttId() {
        return docTypeAttId;
    }

    public String getDocTypeAttDesc() {
        return docTypeAttDesc;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public String getDocTypeAttValue() {
        return docTypeAttValue;
    }

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Integer documentId) {
        this.documentId = documentId;
    }

    public Integer getcategoryId() {
        return categoryId;
    }

    public void setcategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeId(Integer docTypeId) {
        this.docTypeId = docTypeId;
    }


    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DocDetails other = (DocDetails) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocumentId() == null ? other.getDocumentId() == null : this.getDocumentId().equals(other.getDocumentId()))
            && (this.getcategoryId() == null ? other.getcategoryId() == null : this.getcategoryId().equals(other.getcategoryId()))
            && (this.getDocTypeId() == null ? other.getDocTypeId() == null : this.getDocTypeId().equals(other.getDocTypeId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocumentId() == null) ? 0 : getDocumentId().hashCode());
        result = prime * result + ((getcategoryId() == null) ? 0 : getcategoryId().hashCode());
        result = prime * result + ((getDocTypeId() == null) ? 0 : getDocTypeId().hashCode());

        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", documentId=").append(documentId);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", docTypeId=").append(docTypeId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}