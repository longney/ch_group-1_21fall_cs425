package com.dvo.domain;

import java.io.Serializable;

public class Subcate implements Serializable {
    private Integer incrementId;

    private Integer catcategoryId;

    private Integer subcategaryId;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getCatcategoryId() {
        return catcategoryId;
    }

    public void setCatcategoryId(Integer catcategoryId) {
        this.catcategoryId = catcategoryId;
    }

    public Integer getSubcategaryId() {
        return subcategaryId;
    }

    public void setSubcategaryId(Integer subcategaryId) {
        this.subcategaryId = subcategaryId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Subcate other = (Subcate) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getCatcategoryId() == null ? other.getCatcategoryId() == null : this.getCatcategoryId().equals(other.getCatcategoryId()))
            && (this.getSubcategaryId() == null ? other.getSubcategaryId() == null : this.getSubcategaryId().equals(other.getSubcategaryId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getCatcategoryId() == null) ? 0 : getCatcategoryId().hashCode());
        result = prime * result + ((getSubcategaryId() == null) ? 0 : getSubcategaryId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", catcategoryId=").append(catcategoryId);
        sb.append(", subcategaryId=").append(subcategaryId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}