package com.dvo.domain;

import java.io.Serializable;
import java.sql.Timestamp;

public class Borrow implements Serializable {
    private Integer incrementId;

    private Integer docCopyId;

    private Integer personId;

    private Timestamp borrowDate;

    private Timestamp estimatedReturnDate;

    private Timestamp actualReturnDate;

    private String Email;
    private String Doctitlename;

    public String getEmail() {
        return Email;
    }

    public String getDoctitlename() {
        return Doctitlename;
    }

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocCopyId() {
        return docCopyId;
    }

    public void setDocCopyId(Integer docCopyId) {
        this.docCopyId = docCopyId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Timestamp getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Timestamp borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Timestamp getEstimatedReturnDate() {
        return estimatedReturnDate;
    }

    public void setEstimatedReturnDate(Timestamp estimatedReturnDate) {
        this.estimatedReturnDate = estimatedReturnDate;
    }

    public Timestamp getActualReturnDate() {
        return actualReturnDate;
    }

    public void setActualReturnDate(Timestamp actualReturnDate) {
        this.actualReturnDate = actualReturnDate;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Borrow other = (Borrow) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocCopyId() == null ? other.getDocCopyId() == null : this.getDocCopyId().equals(other.getDocCopyId()))
            && (this.getPersonId() == null ? other.getPersonId() == null : this.getPersonId().equals(other.getPersonId()))
            && (this.getBorrowDate() == null ? other.getBorrowDate() == null : this.getBorrowDate().equals(other.getBorrowDate()))
            && (this.getEstimatedReturnDate() == null ? other.getEstimatedReturnDate() == null : this.getEstimatedReturnDate().equals(other.getEstimatedReturnDate()))
            && (this.getActualReturnDate() == null ? other.getActualReturnDate() == null : this.getActualReturnDate().equals(other.getActualReturnDate()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocCopyId() == null) ? 0 : getDocCopyId().hashCode());
        result = prime * result + ((getPersonId() == null) ? 0 : getPersonId().hashCode());
        result = prime * result + ((getBorrowDate() == null) ? 0 : getBorrowDate().hashCode());
        result = prime * result + ((getEstimatedReturnDate() == null) ? 0 : getEstimatedReturnDate().hashCode());
        result = prime * result + ((getActualReturnDate() == null) ? 0 : getActualReturnDate().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", docCopyId=").append(docCopyId);
        sb.append(", personId=").append(personId);
        sb.append(", borrowDate=").append(borrowDate);
        sb.append(", estimatedReturnDate=").append(estimatedReturnDate);
        sb.append(", actualReturnDate=").append(actualReturnDate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}