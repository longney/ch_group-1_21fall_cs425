package com.dvo.domain;

import java.io.Serializable;

public class DocumentCopies implements Serializable {
    private Integer incrementId;

    private Integer docCopyId;

    private Integer docVerId;

    private static final long serialVersionUID = 1L;

    public Integer getIncrementId() {
        return incrementId;
    }

    public void setIncrementId(Integer incrementId) {
        this.incrementId = incrementId;
    }

    public Integer getDocCopyId() {
        return docCopyId;
    }

    public void setDocCopyId(Integer docCopyId) {
        this.docCopyId = docCopyId;
    }

    public Integer getDocVerId() {
        return docVerId;
    }

    public void setDocVerId(Integer docVerId) {
        this.docVerId = docVerId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DocumentCopies other = (DocumentCopies) that;
        return (this.getIncrementId() == null ? other.getIncrementId() == null : this.getIncrementId().equals(other.getIncrementId()))
            && (this.getDocCopyId() == null ? other.getDocCopyId() == null : this.getDocCopyId().equals(other.getDocCopyId()))
            && (this.getDocVerId() == null ? other.getDocVerId() == null : this.getDocVerId().equals(other.getDocVerId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getIncrementId() == null) ? 0 : getIncrementId().hashCode());
        result = prime * result + ((getDocCopyId() == null) ? 0 : getDocCopyId().hashCode());
        result = prime * result + ((getDocVerId() == null) ? 0 : getDocVerId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", incrementId=").append(incrementId);
        sb.append(", docCopyId=").append(docCopyId);
        sb.append(", docVerId=").append(docVerId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}