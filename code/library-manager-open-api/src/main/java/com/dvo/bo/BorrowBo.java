package com.dvo.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.sql.Timestamp;

/**
 * 分类业务对象
 */
@Data
public class BorrowBo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer incrementId;
    private Integer docCopyId;
    private Integer personId;
    private Timestamp borrowDate;
    private Timestamp estimatedReturnDate;
    private Timestamp actualReturnDate;

    private String Email;
    private String Doctitlename;
}