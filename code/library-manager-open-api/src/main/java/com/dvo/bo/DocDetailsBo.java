package com.dvo.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 分类业务对象
 */
@Data
public class DocDetailsBo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer incrementId;
    private Integer documentId;
    private Integer categoryId;
    private Integer docTypeId;

    private String categoryName;
    private String docTypeName;
    private String docTypeCode;

    private Integer docTypeAttId;
    private String docTypeAttDesc;
    private Integer displayOrder;

    private String docTypeAttValue;



    private String publisherName;
    private String publisherAddr;
    private String publisherEmail;
    private String publisherPost;

    private Integer publishCode;
    private Date publishDate;


}