package com.dvo.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 分类业务对象
 */
@Data
public class DocumentsBo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer incrementId;
    private Integer documentId;
    private Integer categoryId;
    private Integer docTypeId;
}