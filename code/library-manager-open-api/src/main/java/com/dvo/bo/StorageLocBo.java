package com.dvo.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 分类业务对象
 */
@Data
public class StorageLocBo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer incrementId;
    private Integer locationId;
    private String locationDesc;
}