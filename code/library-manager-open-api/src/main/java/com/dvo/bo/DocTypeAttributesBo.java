package com.dvo.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 分类业务对象
 */
@Data
public class DocTypeAttributesBo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer incrementId;
    private Integer docTypeAttId;
    private String docTypeAttDesc;
    private Integer displayOrder;
}