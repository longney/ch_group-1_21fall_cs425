package com.dvo.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 分类业务对象
 */
@Data
public class PersonsBo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer incrementId;
    private Integer personId;
    private String email;
    private String password;
    private String firstName;
    private String midName;
    private String lastName;
    private Integer capacity;
    private Integer period;

}