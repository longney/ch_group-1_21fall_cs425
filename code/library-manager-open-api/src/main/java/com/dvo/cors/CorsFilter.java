package com.dvo.cors;

import com.dvo.common.ApiResult;
import com.dvo.common.ResultCode;
import com.dvo.utils.AjaxUtils;
import com.dvo.utils.Audience;
import com.dvo.utils.JwtHelper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CorsFilter implements Filter {

    @Resource
    private Audience audience;

    static final String ORIGIN = "Origin";

    @Override
    public void init(FilterConfig filterConfig) {}

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String origin = request.getHeader(ORIGIN);
        String url = request.getRequestURL().toString();

        response.setHeader("Access-Control-Allow-Origin", origin);
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "PUT, POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "content-type, Authorization");
//        filterChain.doFilter(request, response);
        try {
            String method = request.getMethod();
            if ("OPTIONS".equals(method)
                    || url.indexOf("/login") != -1
                    || url.indexOf("/persons/add") != -1
                    || url.indexOf("/docdetails/list") != -1
                    || url.indexOf("/docdetails/detaillistbyattvalue") != -1
                    || url.indexOf("/swagger") != -1
                    || url.indexOf("/api-docs") != -1
                    || url.indexOf("/webjars/") != -1) {
                filterChain.doFilter(request, response);
            } else {
                String auth = request.getHeader("Authorization");
                if (auth == null || "".equals(auth)) {
                    AjaxUtils.writeJson(ApiResult.error("no token", ResultCode.ERR_NO_TOKEN), response, HttpServletResponse.SC_OK);
                } else {
                    //检测token是否有效
                    if (JwtHelper.getInstance().getAudience() == null) {
                        JwtHelper.getInstance().setAudience(audience);
                    }
                    boolean bool = JwtHelper.getInstance().isTokenExpired(auth);
                    if (bool) {
                        AjaxUtils.writeJson(ApiResult.error("token expire", ResultCode.ERR_TOKEN_EXPIRE), response, HttpServletResponse.SC_OK);
                    } else {
                        filterChain.doFilter(request, response);
                    }
                }
            }
        }catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @Override
    public void destroy() {}
}
