package com.dvo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@MapperScan("com.dvo.mapper")
@SpringBootApplication
@RestController
@EnableScheduling
@EnableTransactionManagement
public class DocMgrApplication {

    public static void main(String[] args) {
        SpringApplication.run( DocMgrApplication.class, args );
    }

    @PostConstruct
    void setDefaultTimezone() {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
    }

}



