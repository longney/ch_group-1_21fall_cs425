package com.dvo.service;

import com.dvo.domain.HasR;
import com.dvo.mapper.HasRMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统服务层
 */
@Slf4j
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class HasRMgrService {

    @Resource
    private HasRMapper hasrmapper;

    /**
     * 查询分类列表
     * @return
     */
    public List<HasR> getHasRList() {
        try {
            List<HasR> list = hasrmapper.selectList();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 查询分类列表
     * @return
     */
    public List<HasR> getHasRList(Integer personid) {
        try {
            List<HasR> list = hasrmapper.selectListByPersonID(personid);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 添加或更新分类
     * @param category
     */
    public void addHasR(HasR category) {
        try {
            if (category != null) {
                if (category.getIncrementId() == null) {
                    hasrmapper.insert(category);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 删除分类
     * @param personid
     */
    public void delHasR(Integer personid) {
        try {
            hasrmapper.deleteByPersonID(personid);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
