package com.dvo.service;

import com.dvo.domain.Documents;
import com.dvo.mapper.DocumentsMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统服务层
 */
@Slf4j
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class DocumentsMgrService {

    @Resource
    private DocumentsMapper documentsMapper;

    /**
     * 查询分类列表
     * @return
     */
    public List<Documents> getDocumentsList() {
        try {
            List<Documents> list = documentsMapper.selectList();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 查询分类列表
     * @return
     */
    public List<Documents> getDocumentsList(Integer Documents_ID) {
        try {
            List<Documents> list = documentsMapper.selectListByID(Documents_ID);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 添加或更新分类
     * @param documents
     */
    public void addOrUpdDocuments(Documents documents) {
        try {
            if (documents != null) {
                if (documents.getIncrementId() == null) {
                    documentsMapper.insert(documents);
                } else {
                    documentsMapper.updateByPrimaryKey(documents);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 删除分类
     * @param documentsId
     */
    public void delDocuments(Integer documentsId) {
        try {
            documentsMapper.deleteByPrimaryKey(documentsId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
