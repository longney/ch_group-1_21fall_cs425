package com.dvo.service;

import com.dvo.domain.Category;
import com.dvo.domain.StorageLocation;
import com.dvo.mapper.CategoryMapper;
import com.dvo.mapper.StorageLocationMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统服务层
 */
@Slf4j
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class StorageLocationService {

    @Resource
    private StorageLocationMapper storageLocMapper;

    /**
     * 查询分类列表
     * @return
     */
    public List<Category> getStorageLocs() {
        try {
            List<Category> list = storageLocMapper.selectList();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 查询分类列表
     * @return
     */
    public List<Category> getStorageLocList(String locationDesc) {
        try {
            List<Category> list = storageLocMapper.selectListByName(locationDesc);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 添加或更新分类
     * @param storageLocation
     */
    public void addOrUpdStorageLoc(StorageLocation storageLocation) {
        try {
            if (storageLocation != null) {
                if (storageLocation.getIncrementId() == null) {
                    storageLocMapper.insert(storageLocation);
                } else {
                    storageLocMapper.updateByPrimaryKey(storageLocation);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 删除分类
     * @param incrementId
     */
    public void delStorageLoc(Integer incrementId) {
        try {
            storageLocMapper.deleteByPrimaryKey(incrementId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
