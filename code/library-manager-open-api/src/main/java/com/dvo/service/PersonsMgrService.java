package com.dvo.service;

import com.dvo.domain.Persons;
import com.dvo.mapper.PersonsMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统服务层
 */
@Slf4j
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class PersonsMgrService {

    @Resource
    private PersonsMapper personsmapper;

    /**
     * 查询分类列表
     * @return
     */
    public List<Persons> getPersonsList() {
        try {
            List<Persons> list = personsmapper.selectList();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 查询分类列表
     * @return
     */
    public List<Persons> getPersonsList(String category_name) {
        try {
            List<Persons> list = personsmapper.selectListByName(category_name);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    public List<Persons> getPersonsListByID(Integer ID) {
        try {
            List<Persons> list = personsmapper.selectListByID(ID);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    public List<Persons> getPersonsLogin(String person_id ,String person_password) {
        try {
            List<Persons> list = personsmapper.selectPersonsLogin(person_id,person_password);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 添加或更新分类
     * @param persons
     */
    public void addOrUpdPersons(Persons persons) {
        try {
            if (persons != null) {
                if (persons.getIncrementId() == null) {
                    personsmapper.insert(persons);
                } else {
                    personsmapper.updateByPrimaryKey(persons);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 删除分类
     * @param personsId
     */
    public void delPersons(Integer personsId) {
        try {
            personsmapper.deleteByPrimaryKey(personsId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
