package com.dvo.service;

import com.dvo.domain.DocTypeAttributes;
import com.dvo.mapper.DocTypeAttributesMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统服务层
 */
@Slf4j
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class DocTypeAttributesMgrService {

    @Resource
    private DocTypeAttributesMapper docTypeAttributesMapper;

    /**
     * 查询分类列表
     * @return
     */
    public List<DocTypeAttributes> getCategoryList() {
        try {
            List<DocTypeAttributes> list = docTypeAttributesMapper.selectList();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 查询分类列表
     * @return
     */
    public List<DocTypeAttributes> getCategoryList(String category_name) {
        try {
            List<DocTypeAttributes> list = docTypeAttributesMapper.selectListByName(category_name);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 添加或更新分类
     * @param doctypeattribute
     */
    public void addOrUpdDocTypeAttributes(DocTypeAttributes doctypeattribute) {
        try {
            if (doctypeattribute != null) {
                if (doctypeattribute.getIncrementId() == null) {
                    docTypeAttributesMapper.insert(doctypeattribute);
                } else {
                    docTypeAttributesMapper.updateByPrimaryKey(doctypeattribute);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 删除分类
     * @param docTypeAttId
     */
    public void delDocTypeAttributes(Integer docTypeAttId) {
        try {
            docTypeAttributesMapper.deleteByPrimaryKey(docTypeAttId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
