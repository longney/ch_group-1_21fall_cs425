package com.dvo.service;

import com.dvo.domain.Borrow;
import com.dvo.mapper.BorrowMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统服务层
 */
@Slf4j
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class BorrowMgrService {

    @Resource
    private BorrowMapper borrowMapper;


    /**
     * 查询分类列表
     * @return
     */
    public List<Borrow> getBorrowList(Integer PersonID) {
        try {
            List<Borrow> list = borrowMapper.selectListByPersonID(PersonID);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 添加或更新分类
     * @param borrow
     */
    public void addOrUpdBorrow(Borrow borrow) {
        try {
            if (borrow != null) {
                if (borrow.getIncrementId() == null) {
                    borrowMapper.insert(borrow);
                } else {
                    borrowMapper.returnDoc(borrow);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }



}
