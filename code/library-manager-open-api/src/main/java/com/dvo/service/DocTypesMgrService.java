package com.dvo.service;

import com.dvo.domain.DocTypes;
import com.dvo.mapper.DocTypesMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统服务层
 */
@Slf4j
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class DocTypesMgrService {

    @Resource
    private DocTypesMapper doctypesmapper;

    /**
     * 查询分类列表
     * @return
     */
    public List<DocTypes> getDocTypesList() {
        try {
            List<DocTypes> list = doctypesmapper.selectList();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 查询分类列表
     * @return
     */
    public List<DocTypes> getDocTypesList(String doctypes_name) {
        try {
            List<DocTypes> list = doctypesmapper.selectListByName(doctypes_name);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 添加或更新分类
     * @param doctypes
     */
    public void addOrUpdDocTypes(DocTypes doctypes) {
        try {
            if (doctypes != null) {
                if (doctypes.getIncrementId() == null) {
                    doctypesmapper.insert(doctypes);
                } else {
                    doctypesmapper.updateByPrimaryKey(doctypes);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 删除分类
     * @param doctypesId
     */
    public void delDocTypes(Integer doctypesId) {
        try {
            doctypesmapper.deleteByPrimaryKey(doctypesId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
