package com.dvo.service;

import com.dvo.domain.DocDetails;
import com.dvo.mapper.DocDetailsMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统服务层
 */
@Slf4j
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class DocDetailsMgrService {

    @Resource
    private DocDetailsMapper docdetailsMapper;

    /**
     * 查询分类列表
     * @return
     */
    public List<DocDetails> getDocDetailsList() {
        try {
            List<DocDetails> list = docdetailsMapper.selectList();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 查询分类列表
     * @return
     */
    public List<DocDetails> getDocumentsList() {
        try {
            List<DocDetails> list = docdetailsMapper.selectDocumentsList();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 查询分类列表
     * @return
     */
    public List<DocDetails> getDocumentsList(Integer Documents_ID) {
        try {
            List<DocDetails> list = docdetailsMapper.selectListByID(Documents_ID);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 查询分类列表
     * @return
     */
    public List<DocDetails> getDocSearchList(String DOC_TYPE_ATT_VALUE) {
        try {
            List<DocDetails> list = docdetailsMapper.selectListByString(DOC_TYPE_ATT_VALUE);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
