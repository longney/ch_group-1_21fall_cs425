package com.dvo.service;

import com.dvo.domain.HasD;
import com.dvo.mapper.HasDMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统服务层
 */
@Slf4j
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class HasDMgrService {

    @Resource
    private HasDMapper hasdmapper;

    /**
     * 查询分类列表
     * @return
     */
    public List<HasD> getHasDList() {
        try {
            List<HasD> list = hasdmapper.selectList();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 查询分类列表
     * @return
     */
    public List<HasD> getHasDList(Integer personid) {
        try {
            List<HasD> list = hasdmapper.selectListByDocumentID(personid);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 添加或更新分类
     * @param category
     */
    public void addHasD(HasD category) {
        try {
            if (category != null) {
                if (category.getIncrementId() == null) {
                    hasdmapper.insert(category);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 删除分类
     * @param personid
     */
    public void delHasD(Integer personid) {
        try {
            hasdmapper.deleteByDocumentID(personid);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
