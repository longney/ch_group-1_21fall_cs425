package com.dvo.service;

import com.dvo.domain.Category;
import com.dvo.mapper.CategoryMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统服务层
 */
@Slf4j
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class DocMgrService {

    @Resource
    private CategoryMapper categoryMapper;

    /**
     * 查询分类列表
     * @return
     */
    public List<Category> getCategoryList() {
        try {
            List<Category> list = categoryMapper.selectList();
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 查询分类列表
     * @return
     */
    public List<Category> getCategoryList(String category_name) {
        try {
            List<Category> list = categoryMapper.selectListByName(category_name);
            return list;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }
    /**
     * 添加或更新分类
     * @param category
     */
    public void addOrUpdCategory(Category category) {
        try {
            if (category != null) {
                if (category.getIncrementId() == null) {
                    categoryMapper.insert(category);
                } else {
                    categoryMapper.updateByPrimaryKey(category);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 删除分类
     * @param categoryId
     */
    public void delCategory(Integer categoryId) {
        try {
            categoryMapper.deleteByPrimaryKey(categoryId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
