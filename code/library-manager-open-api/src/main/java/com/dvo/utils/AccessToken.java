package com.dvo.utils;

import lombok.Data;

@Data
public class AccessToken {
	private String token;
	private String bearer;
	private long expires_in;
	private String introduction;
	private String avatar;

	public String getToken(){
	return this.token;
}
}