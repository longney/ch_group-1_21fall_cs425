package com.dvo.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "audience")
@PropertySource("classpath:public/jwt.properties")
public class Audience {
	// for jwt
	private String clientId;
	private String base64Secret;
	private String name;
	private int expiresSecond;

	// for aes
	private String secret;
	private int keySize;
	private int iterationCount;
	private String salt;
	private String iv;
}
