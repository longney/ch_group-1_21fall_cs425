package com.dvo.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 发起https请求
 */
public class Https {
    private static final Logger logger = LoggerFactory.getLogger(Https.class);
    public static final String METHOD_POST = "POST";
    public static final String METHOD_GET = "GET";
    public static final String ENCODING_CHARSET = "utf-8";

    public static String executeGet(String url) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet httpget = new HttpGet(url);
            CloseableHttpResponse resp = httpclient.execute(httpget);
            return EntityUtils.toString(resp.getEntity());
        } catch (Exception e) {
            logger.error("Failed to connect to url ", e);
        } finally {
            try {
                httpclient.close();
            } catch (IOException e) {
            }
        }
        return "";
    }

    public static String executeDel(String url) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpDelete http = new HttpDelete(url);
            http.addHeader(HTTP.CONTENT_TYPE, "application/json");
            CloseableHttpResponse resp = httpclient.execute(http);
            return EntityUtils.toString(resp.getEntity());
        } catch (Exception e) {
            logger.error("Failed to connect to url ", e);
        } finally {
            try {
                httpclient.close();
            } catch (IOException e) {
            }
        }
        return "";
    }

    public static String executePost(String url, Map<String, ? extends Object> param) {
        String result = "";
        try {
            HttpPost post = new HttpPost(url);
            CloseableHttpClient client = HttpClients.createDefault();
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            for (Map.Entry<String, ? extends Object> entry : param.entrySet()) {
                if (entry.getValue() != null) {
                    params.add(new BasicNameValuePair(entry.getKey(), String.valueOf(entry
                            .getValue())));
                }
            }
            HttpEntity formEntity = new UrlEncodedFormEntity(params, "UTF-8");
            post.addHeader(HTTP.CONTENT_TYPE, "application/json");
            post.setEntity(formEntity);
            HttpResponse response = client.execute(post);
            InputStream is = response.getEntity().getContent();
            result = inStream2String(is);
        } catch (Exception e) {
            logger.error("Failed to connect to url ,{}", e, url);
        }
        return result;
    }

    public static String executePut(String url, Map<String, ? extends Object> param) {
        String result = "";
        try {
            HttpPut post = new HttpPut(url);
            CloseableHttpClient client = HttpClients.createDefault();
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            for (Map.Entry<String, ? extends Object> entry : param.entrySet()) {
                if (entry.getValue() != null) {
                    params.add(new BasicNameValuePair(entry.getKey(), String.valueOf(entry
                            .getValue())));
                }
            }
            HttpEntity formEntity = new UrlEncodedFormEntity(params, "UTF-8");
            post.addHeader(HTTP.CONTENT_TYPE, "application/json");
            post.setEntity(formEntity);
            HttpResponse response = client.execute(post);
            InputStream is = response.getEntity().getContent();
            result = inStream2String(is);
        } catch (Exception e) {
            logger.error("Failed to connect to url ,{}", e, url);
        }
        return result;
    }

    public static String executeGet(String url, Map<String, String> param) {
        // 创建Httpclient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String resultString = "";
        try {
            // 创建uri
            URIBuilder builder = new URIBuilder(url);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建Http Delete请求
            HttpGet httpGet = new HttpGet(uri);
            // 执行http请求
            response = httpClient.execute(httpGet);
            resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (Exception e) {
            logger.error("Failed to connect to url ,{}", e);

        } finally {
            try {
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                logger.error("Failed to connect to url ,{}", e);
            }
        }
        return resultString;
    }

    public static String doDelete(String url, Map<String, String> param) {
        // 创建Httpclient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String resultString = "";
        try {
            // 创建uri
            URIBuilder builder = new URIBuilder(url);
            if (param != null) {
                for (String key : param.keySet()) {
                    builder.addParameter(key, param.get(key));
                }
            }
            URI uri = builder.build();
            // 创建Http Delete请求
            HttpDelete httpDelete = new HttpDelete(uri);
            // 执行http请求
            response = httpClient.execute(httpDelete);
            resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (Exception e) {
            logger.error("Failed to connect to url ,{}", e);

        } finally {
            try {
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                logger.error("Failed to connect to url ,{}", e);
            }
        }
        return resultString;
    }


    // 将输入流转换成字符串
    private static String inStream2String(InputStream is) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int len = -1;
        while ((len = is.read(buf)) != -1) {
            baos.write(buf, 0, len);
        }
        return new String(baos.toByteArray());
    }

    public static String reqJsonPost(HttpServletRequest request, String method, String reqjson) throws IOException {
        return reqJsonPost(doReqUrl(request.getRequestURL(), "saveRouteDb"), reqjson);
    }

    public static String reqJsonPost(String url, String reqjson) throws IOException {
        URL httpclient = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) httpclient.openConnection();
        conn.setConnectTimeout(5000);
        conn.setReadTimeout(2000);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.connect();
        OutputStream os = conn.getOutputStream();
        os.write(reqjson.getBytes("UTF-8"));
        os.flush();
        os.close();
        InputStream is = conn.getInputStream();
        int size = is.available();
        byte[] jsonBytes = new byte[size];
        is.read(jsonBytes);
        String message = new String(jsonBytes, "UTF-8");
        logger.info(message);
        return message;
    }

    public static String doReqUrl(StringBuffer url, String method) {
        return url.substring(0, url.lastIndexOf("/") + 1).concat(method);
    }

    /**
     * 拿到请求的body内容
     *
     * @param request
     * @return
     * @throws IOException
     */
    public static String getReqBody(HttpServletRequest request) throws IOException {
        return IOUtils.toString(request.getInputStream(), ENCODING_CHARSET);
    }

    /**
     * 反序列号对象
     *
     * @param request
     * @param clz
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> T getReqObject(HttpServletRequest request, Class<T> clz) throws IOException {
        return JSONObject.parseObject(getReqBody(request), clz);
    }

    /**
     * jsonpost方式提交
     *
     * @param request
     * @param method
     * @param jsonstr
     * @return
     * @throws IOException
     */
    public static JSONObject doJsonPost(HttpServletRequest request, String method, String jsonstr) throws IOException {
        return doJsonPost(doReqUrl(request.getRequestURL(), method), jsonstr);
    }

    /**
     * POST方式发送json数据串
     * 客户端和服务器端两边json数据传输
     *
     * @param url
     * @param jsonstr
     * @return
     * @throws IOException
     */
    public static JSONObject doJsonPost(String url, String jsonstr) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
//        httpPost.getParams().setIntParameter("http.socket.timeout", 30000);
        httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json");
        StringEntity se = new StringEntity(jsonstr, "UTF-8");
//        se.setContentType("text/json");
        se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(50000).setConnectionRequestTimeout(30000)
                .setSocketTimeout(50000).build();
        httpPost.setConfig(requestConfig);
        httpPost.setEntity(se);
        JSONObject obj = null;
        HttpResponse response = httpClient.execute(httpPost);
        if (response != null && response.getStatusLine().getStatusCode() == 200) {
            String result = EntityUtils.toString(response.getEntity());
            // 生成 JSON 对象
            obj = JSONObject.parseObject(result);
            // get token
            Header[] headers = response.getHeaders("Authorization");
            for (Header h : headers) {
                obj.put(h.getName(), h.getValue());
            }
            String errorcode = obj.getString("errorcode");
            if ("000".equals(errorcode)) {
                logger.info("调用成功>>>>>>>>>");
            }
        }
        return obj;
    }

    /**
     * POST方式发送json数据串
     * 客户端和服务器端两边json数据传输
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static JSONObject doJsonPostByHeader(String url, String authorization) throws IOException {
        // 1.拿到一个httpclient的对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // 2.设置请求方式和请求信息
        HttpPost httpPost = new HttpPost(url);

        //2.1 提交header头信息
        httpPost.addHeader("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36");
        httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json");
        httpPost.addHeader("Authorization", authorization);
        // 3.执行请求
        CloseableHttpResponse response = httpClient.execute(httpPost);
        String result = EntityUtils.toString(response.getEntity(), Charset.forName("utf-8"));
        // 生成 JSON 对象
        JSONObject obj = JSONObject.parseObject(result);
        // get new token
        Header[] headers = response.getHeaders("Authorization");
        for (Header h : headers) {
            obj.put(h.getName(), h.getValue());
        }
        String errorcode = obj.getString("errorcode");
        if ("000".equals(errorcode)) {
            logger.info("调用成功>>>>>>>>>");
        }
        return obj;
    }

    /**
     * POST方式发送json数据串
     * 客户端和服务器端两边json数据传输
     *
     * @param url
     * @param jsonstr
     * @return
     * @throws IOException
     */
    public static JSONObject doJsonPostByHeader2(String url, String jsonstr, String authorization) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
//        httpPost.getParams().setIntParameter("http.socket.timeout", 30000);
        httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json");
        httpPost.addHeader("Authorization", authorization);
        StringEntity se = new StringEntity(jsonstr, "UTF-8");
//        se.setContentType("text/json");
        se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(50000).setConnectionRequestTimeout(30000)
                .setSocketTimeout(50000).build();
        httpPost.setConfig(requestConfig);
        httpPost.setEntity(se);
        JSONObject obj = null;
        HttpResponse response = httpClient.execute(httpPost);
        if (response != null && response.getStatusLine().getStatusCode() == 200) {
            String result = EntityUtils.toString(response.getEntity());
            // 生成 JSON 对象
            obj = JSONObject.parseObject(result);
            // get new token
            Header[] headers = response.getHeaders("Authorization");
            for (Header h : headers) {
                obj.put(h.getName(), h.getValue());
            }
            String errorcode = obj.getString("errorcode");
            if ("000".equals(errorcode)) {
                logger.info("调用成功>>>>>>>>>");
            }
        }
        return obj;
    }

    public static JSONObject doJsonPut(String url, String jsonstr) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPut httpPost = new HttpPut(url);
        httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json");
        StringEntity se = new StringEntity(jsonstr, "UTF-8");
        se.setContentType("text/json");
        se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(5000).setConnectionRequestTimeout(1000)
                .setSocketTimeout(5000).build();
        httpPost.setConfig(requestConfig);
        httpPost.setEntity(se);
        JSONObject obj = null;
        HttpResponse response = httpClient.execute(httpPost);
        if (response != null && response.getStatusLine().getStatusCode() == 200) {
            String result = EntityUtils.toString(response.getEntity());
            // 生成 JSON 对象
            obj = JSONObject.parseObject(result);
            String errorcode = obj.getString("errorcode");
            if ("000".equals(errorcode)) {
                logger.info("调用成功>>>>>>>>>");
            }
        }
        return obj;
    }

    public static String doPutOfJson(String url, String json) {
        // 创建Httpclient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String resultString = "";
        try {
            // 创建Http Put请求
            HttpPut httpPut = new HttpPut(url);
            // 创建请求内容
            StringEntity entity = new StringEntity(json, ContentType.APPLICATION_JSON);
            httpPut.setEntity(entity);
            // 执行http请求
            response = httpClient.execute(httpPut);
            resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (Exception e) {
            logger.error("", e);

        } finally {
            try {
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                logger.error("", e);
            }
        }

        return resultString;
    }
}
