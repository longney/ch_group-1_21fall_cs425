package com.dvo.utils;

import io.jsonwebtoken.*;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtUtils {

	/**
	 * token 过期时间, 单位: 毫秒. 这个值表示20分钟 20 * 60 * 1000;
	 */
	private static long accessTokenExpireTime;

	/**
	 * jwt 加密解密密钥 MDk4ZjZiY2Q0NjIxZDD3D2NhGGU0ZTgzMjYyN2I0ZjY=
	 */
	private static String encryptJWTKey="MDk4ZjZiY2Q0NjIxZDD3D2NhGGU0ZTgzMjYyN2I0ZjY=";

	public static final String jwtId = "tokenId";

	/**
	 * 创建JWT
	 */
	public static String createJWT(Map<String, Object> claims, Long time) {
		// 指定签名的时候使用的签名算法，也就是header那部分，jjwt已经将这部分内容封装好了。
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		Date now = new Date(System.currentTimeMillis());

		SecretKey secretKey = generalKey();
		// 生成JWT的时间
		long nowMillis = System.currentTimeMillis();
		// 下面就是在为payload添加各种标准声明和私有声明了
		JwtBuilder builder = Jwts.builder()
				.setClaims(claims)
				.setId(jwtId)
				.setIssuedAt(now)
				.signWith(signatureAlgorithm, secretKey);
		if (time >= 0) {
			long expMillis = nowMillis + time;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}
		return builder.compact();
	}

	/**
	 * 验证jwt
	 */
	public static Claims verifyJwt(String token) throws SignatureException,
			ExpiredJwtException, MalformedJwtException {
		// 签名秘钥，和生成的签名的秘钥一模一样
		SecretKey key = generalKey();
		Claims claims;
		claims = Jwts.parser()
				.setSigningKey(key)
				.parseClaimsJws(token).getBody();
		// 设置需要解析的jwt
		return claims;
	}

	/**
	 * 由字符串生成加密key
	 * @return
	 */
	public static SecretKey generalKey() {

		byte[] encodedKey = Base64.decodeBase64(encryptJWTKey);
		SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length,
				"AES");
		return key;
	}

	/**
	 * 根据userId和openid生成token
	 */
	public static String generateToken(Integer userId) {
		Map<String, Object> map = new HashMap<>();
		map.put("userId", userId);
		return createJWT(map, accessTokenExpireTime);
	}

	public static String getValue(String token, String value) {
		Claims claims = verifyJwt(token);
		if (null != claims) {
			return claims.get(value).toString();
		} else {
			return null;
		}
	}

}
