package com.dvo.mapper;

import com.dvo.domain.DocTypeAttributes;
import java.util.List;
public interface DocTypeAttributesMapper extends GenericMapper<DocTypeAttributes, Integer> {
    List<DocTypeAttributes> selectList();
    List<DocTypeAttributes> selectListByName(String doc_type_att_desc);
}