package com.dvo.mapper;

import com.dvo.domain.Writes;
import java.util.List;
public interface WritesMapper extends GenericMapper<Writes, Integer> {
}