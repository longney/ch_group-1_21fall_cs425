package com.dvo.mapper;

import com.dvo.domain.Category;

import java.util.List;

public interface CategoryMapper extends GenericMapper<Category, Integer> {

    List<Category> selectList();
    List<Category> selectListByName(String category_name);
}