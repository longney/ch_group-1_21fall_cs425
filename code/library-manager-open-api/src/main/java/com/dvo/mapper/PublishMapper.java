package com.dvo.mapper;

import com.dvo.domain.Publish;
import java.util.List;
public interface PublishMapper extends GenericMapper<Publish, Integer> {
}