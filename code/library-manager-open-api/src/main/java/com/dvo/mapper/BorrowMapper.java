package com.dvo.mapper;

import com.dvo.domain.Borrow;
import java.util.List;
public interface BorrowMapper extends GenericMapper<Borrow, Integer> {
    List<Borrow> selectListByPersonID(Integer PersonID);
    void returnDoc (Borrow borrowReturn);
}