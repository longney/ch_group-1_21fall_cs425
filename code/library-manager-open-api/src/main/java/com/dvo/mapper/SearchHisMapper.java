package com.dvo.mapper;

import com.dvo.domain.SearchHis;
import java.util.List;
public interface SearchHisMapper extends GenericMapper<SearchHis, Integer> {
}