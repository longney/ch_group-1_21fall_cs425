package com.dvo.mapper;

import com.dvo.domain.DocumentCopies;
import java.util.List;
public interface DocumentCopiesMapper extends GenericMapper<DocumentCopies, Integer> {
}