package com.dvo.mapper;

import com.dvo.domain.Rights;
import java.util.List;
public interface RightsMapper extends GenericMapper<Rights, Integer> {
}