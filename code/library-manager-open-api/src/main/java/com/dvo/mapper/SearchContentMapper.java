package com.dvo.mapper;

import com.dvo.domain.SearchContent;
import java.util.List;
public interface SearchContentMapper extends GenericMapper<SearchContent, Integer> {
}