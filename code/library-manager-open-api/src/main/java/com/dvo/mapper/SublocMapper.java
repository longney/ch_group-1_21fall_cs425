package com.dvo.mapper;

import com.dvo.domain.Subloc;
import java.util.List;
public interface SublocMapper extends GenericMapper<Subloc, Integer> {
}