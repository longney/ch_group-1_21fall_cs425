package com.dvo.mapper;

import com.dvo.domain.Category;
import com.dvo.domain.StorageLocation;
import java.util.List;
public interface StorageLocationMapper extends GenericMapper<StorageLocation, Integer> {

    List<Category> selectList();
    List<Category> selectListByName(String category_name);
}