package com.dvo.mapper;

import com.dvo.domain.Publisher;
import java.util.List;
public interface PublisherMapper extends GenericMapper<Publisher, Integer> {
}