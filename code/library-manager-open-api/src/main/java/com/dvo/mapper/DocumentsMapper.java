package com.dvo.mapper;

import com.dvo.domain.Documents;
import java.util.List;
public interface DocumentsMapper extends GenericMapper<Documents, Integer> {
    List<Documents> selectList();
    List<Documents> selectListByID(Integer Documents_ID);
}