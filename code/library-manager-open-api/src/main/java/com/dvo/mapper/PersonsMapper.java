package com.dvo.mapper;

import com.dvo.domain.Persons;
import java.util.List;
public interface PersonsMapper extends GenericMapper<Persons, Integer> {
    List<Persons> selectList();
    List<Persons> selectListByName(String name);
    List<Persons> selectListByID(Integer ID);
    List<Persons> selectPersonsLogin(String email,String password);
}