package com.dvo.mapper;

import com.dvo.domain.Reference;
import java.util.List;
public interface ReferenceMapper extends GenericMapper<Reference, Integer> {
}