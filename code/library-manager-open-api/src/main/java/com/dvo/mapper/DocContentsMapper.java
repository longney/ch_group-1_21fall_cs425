package com.dvo.mapper;

import com.dvo.domain.DocContents;
import java.util.List;
public interface DocContentsMapper extends GenericMapper<DocContents, Integer> {
}