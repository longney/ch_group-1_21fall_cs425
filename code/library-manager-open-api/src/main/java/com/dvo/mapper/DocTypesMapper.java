package com.dvo.mapper;

import com.dvo.domain.DocTypes;
import java.util.List;
public interface DocTypesMapper extends GenericMapper<DocTypes, Integer> {
    List<DocTypes> selectList();
    List<DocTypes> selectListByName(String doctypes_name);
}