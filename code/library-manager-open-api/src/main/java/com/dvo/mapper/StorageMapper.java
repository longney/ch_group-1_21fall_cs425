package com.dvo.mapper;

import com.dvo.domain.Storage;
import java.util.List;
public interface StorageMapper extends GenericMapper<Storage, Integer> {
}