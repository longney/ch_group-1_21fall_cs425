package com.dvo.mapper;

import com.dvo.domain.DocumentVersion;
import java.util.List;
public interface DocumentVersionMapper extends GenericMapper<DocumentVersion, Integer> {
}