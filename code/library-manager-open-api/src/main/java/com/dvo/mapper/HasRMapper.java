package com.dvo.mapper;

import com.dvo.domain.HasR;
import java.util.List;
public interface HasRMapper extends GenericMapper<HasR, Integer> {
    List<HasR> selectList();
    List<HasR> selectListByPersonID(Integer personid);
    void deleteByPersonID(Integer personid);

}