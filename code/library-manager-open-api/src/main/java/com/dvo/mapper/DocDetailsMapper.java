package com.dvo.mapper;

import com.dvo.domain.DocDetails;

import java.util.List;

public interface DocDetailsMapper extends GenericMapper<DocDetails, Integer> {
    List<DocDetails> selectList();
    List<DocDetails> selectDocumentsList();
    List<DocDetails> selectListByID(Integer Documents_ID);
    List<DocDetails> selectListByString(String Search_Value);
}