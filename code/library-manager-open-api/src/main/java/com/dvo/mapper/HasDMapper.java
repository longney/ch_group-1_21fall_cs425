package com.dvo.mapper;


import com.dvo.domain.HasD;

import java.util.List;
public interface HasDMapper extends GenericMapper<HasD, Integer> {
    List<HasD> selectList();
    List<HasD> selectListByDocumentID(Integer documentid);
    void deleteByDocumentID(Integer documentid);
}