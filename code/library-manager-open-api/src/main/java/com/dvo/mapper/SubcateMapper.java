package com.dvo.mapper;

import com.dvo.domain.Subcate;
import java.util.List;
public interface SubcateMapper extends GenericMapper<Subcate, Integer> {
}