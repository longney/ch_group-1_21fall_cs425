package com.dvo.controller;

import com.dvo.bo.DocumentsBo;
import com.dvo.common.ApiResult;
import com.dvo.domain.Documents;
import com.dvo.service.DocumentsMgrService;
import com.dvo.utils.Audience;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统控制器
 */
@Api("文档管理系统控制器")
@Slf4j
@RestController
public class DocumentsMgrController {
    @Resource
    private Audience audience;

    @Resource
    private DocumentsMgrService documentsMgrService;



    /**
     * 分类列表查询
     */
    @ApiOperation("文档列表查询")
    @GetMapping("/documents/list")
    public ApiResult selectDocumentsList() {
        try {
            List<Documents> list = documentsMgrService.getDocumentsList();
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类列表查询
     */
    @ApiOperation("文档列表查询按名称")
    @GetMapping("/documents/listbyid")
    public ApiResult selectDocumentsListByID(@RequestParam Integer documents_id) {
        try {
            List<Documents> list = documentsMgrService.getDocumentsList(documents_id);
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类添加
     */
    @ApiOperation("文档添加")
    @PostMapping("/documents/add")
    public ApiResult addDocuments(@RequestBody DocumentsBo documentsBo) {
        //log.info("addCategory:{},{},{}", categoryBo);
        //Assert.notNull(categoryBo.getCategoryName(), "分类的名字不能为空!");
        try {
            Documents category = new Documents();
            BeanUtils.copyProperties(documentsBo, category);
            documentsMgrService.addOrUpdDocuments(category);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类修改
     */
    @ApiOperation("文档修改")
    @PostMapping("/documents/update")
    public ApiResult updDocuments(@RequestBody DocumentsBo documentsBo) {

        try {
            Documents category = new Documents();
            BeanUtils.copyProperties(documentsBo, category);
            documentsMgrService.addOrUpdDocuments(category);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类删除
     */
    @ApiOperation("文档删除")
    @PostMapping("/documents/del")
    public ApiResult delDocuments(@RequestParam Integer incrementId) {
        log.info("delCategory:{},{},{}", incrementId);
        try {
            documentsMgrService.delDocuments(incrementId);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

}
