package com.dvo.controller;

import com.dvo.bo.StorageLocBo;
import com.dvo.common.ApiResult;
import com.dvo.domain.Category;
import com.dvo.domain.StorageLocation;
import com.dvo.service.StorageLocationService;
import com.dvo.utils.Audience;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统控制器
 */
@Api("存储位置系统控制器")
@Slf4j
@RestController
public class StorageLocationController {
    @Resource
    private Audience audience;

    @Resource
    private StorageLocationService storageLocService;

    /**
     * 存储位置列表查询
     */
    @ApiOperation("存储位置列表查询")
    @GetMapping("/storageLoc/list")
    public ApiResult selectStorageLocationList() {
        try {
            List<Category> list = storageLocService.getStorageLocs();
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 存储位置列表查询
     */
    @ApiOperation("存储位置列表查询按名称")
    @GetMapping("/storageLoc/listbyname")
    public ApiResult selectStorageLocationListByName(@RequestParam String locationDesc) {
        try {
            List<Category> list = storageLocService.getStorageLocList(locationDesc);
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 存储位置添加
     */
    @ApiOperation("存储位置添加")
    @PostMapping("/storageLoc/add")
    public ApiResult addStorageLocation(@RequestBody StorageLocBo storageLocBo) {
        try {
            StorageLocation storageLocation = new StorageLocation();
            BeanUtils.copyProperties(storageLocBo, storageLocation);
            storageLocService.addOrUpdStorageLoc(storageLocation);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类修改
     */
    @ApiOperation("存储位置修改")
    @PostMapping("/storageLoc/update")
    public ApiResult updStorageLocation(@RequestBody StorageLocBo storageLocBo) {
        log.info("updCategory:{},{},{}", storageLocBo);
        Assert.notNull(storageLocBo.getIncrementId(), "存储位置的id不能为空!");
        Assert.notNull(storageLocBo.getLocationId(), "存储位置的LocationId不能为空!");
        Assert.notNull(storageLocBo.getLocationDesc(), "存储位置的描述不能为空!");
        try {
            StorageLocation storageLocation = new StorageLocation();
            BeanUtils.copyProperties(storageLocBo, storageLocation);
            storageLocService.addOrUpdStorageLoc(storageLocation);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 存储位置删除
     */
    @ApiOperation("存储位置删除")
    @PostMapping("/storageLoc/del")
    public ApiResult delStorageLocation(@RequestParam Integer incrementId) {
        log.info("delCategory:{},{},{}", incrementId);
        try {
            storageLocService.delStorageLoc(incrementId);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

}
