package com.dvo.controller;

import com.dvo.bo.DocTypeAttributesBo;
import com.dvo.common.ApiResult;
import com.dvo.domain.DocTypeAttributes;
import com.dvo.service.DocTypeAttributesMgrService;
import com.dvo.utils.Audience;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统控制器
 */
@Api("文档管理系统控制器")
@Slf4j
@RestController
public class DocTypeAttributesMgrController {
    @Resource
    private Audience audience;

    @Resource
    private DocTypeAttributesMgrService docTypeAttributesMgrService;

        /**
     * 分类列表查询
     */
    @ApiOperation("文档类型属性列表查询")
    @GetMapping("/doctypeatt/list")
    public ApiResult selectDocTypeAttList() {
        try {
            List<DocTypeAttributes> list = docTypeAttributesMgrService.getCategoryList();
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类列表查询
     */
    @ApiOperation("文档类型属性列表查询按名称")
    @GetMapping("/doctypeatt/listbyname")
    public ApiResult selectDocTypeAttListByName(@RequestParam String docTypeName) {
        try {
            List<DocTypeAttributes> list = docTypeAttributesMgrService.getCategoryList(docTypeName);
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类添加
     */
    @ApiOperation("文档类型属性添加")
    @PostMapping("/doctypeatt/add")
    public ApiResult adddoctypeatt(@RequestBody DocTypeAttributesBo categoryBo) {
        //log.info("addCategory:{},{},{}", categoryBo);
        try {
            DocTypeAttributes docTypeAttributes = new DocTypeAttributes();
            BeanUtils.copyProperties(categoryBo, docTypeAttributes);
            docTypeAttributesMgrService.addOrUpdDocTypeAttributes(docTypeAttributes);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类修改
     */
    @ApiOperation("文档类型属性修改")
    @PostMapping("/doctypeatt/update")
    public ApiResult upddoctypeatt(@RequestBody DocTypeAttributesBo categoryBo) {
        log.info("updCategory:{},{},{}", categoryBo);

        try {
            DocTypeAttributes docTypeAttributes = new DocTypeAttributes();
            BeanUtils.copyProperties(categoryBo, docTypeAttributes);
            docTypeAttributesMgrService.addOrUpdDocTypeAttributes(docTypeAttributes);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类删除
     */
    @ApiOperation("文档类型属性删除")
    @PostMapping("/doctypeatt/del")
    public ApiResult deldoctypeatt(@RequestParam Integer incrementId) {
        log.info("delCategory:{},{},{}", incrementId);
        try {
            docTypeAttributesMgrService.delDocTypeAttributes(incrementId);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

}
