package com.dvo.controller;

import com.dvo.bo.PersonsBo;
import com.dvo.common.ApiResult;
import com.dvo.domain.HasR;
import com.dvo.service.HasRMgrService;
import com.dvo.domain.Persons;
import com.dvo.service.PersonsMgrService;
import com.dvo.utils.AccessToken;
import com.dvo.utils.Audience;
import com.dvo.utils.JwtHelper;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 文档管理系统控制器
 */
@Api("文档管理系统控制器")
@Slf4j
@RestController
public class PersonsMgrController {
    @Resource
    private Audience audience;

    @Resource
    private PersonsMgrService personsMgrService;

    @Resource
    private HasRMgrService hasrMgrService;
    /**
     * 登录
     */
    @ApiOperation("登录")
    @GetMapping("/login")
    public ApiResult login(@RequestParam String person_name ,@RequestParam String person_password ) {
        try {
            Integer userId=0 ;
            String name="";
            String role="";

            List<Persons> list = personsMgrService.getPersonsLogin(person_name ,person_password);
            Persons person;
            if (list.size() == 1) {
                person = list.get(0);
                userId = person.getIncrementId();
                name = person.getFirstName()+" "+person.getMidName()+" "+person.getLastName();
                List<HasR> Rlist = hasrMgrService.getHasRList(Integer.valueOf(userId));
                Iterator<HasR> iter = Rlist.iterator();
                while (iter.hasNext()) {
                    HasR s = (HasR) iter.next();
                    role=role+"," + s.getRightId();
                }
                //role=Rlist.toString();

                // 根据用户信息生成token
                if (JwtHelper.getInstance().getAudience() == null) {
                    JwtHelper.getInstance().setAudience(audience);
                }
                AccessToken token = JwtHelper.getInstance().createToken(name, String.valueOf(userId), role);
                //Claims p=JwtHelper.getInstance().parseJWT(token.getToken());
                return ApiResult.success(token);
            }else return ApiResult.error("No Person found!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }
    /**
     * token解析
     */
    @ApiOperation("token解析")
    @GetMapping("/tokenParse")
    public ApiResult tokenParse(String token) {
        try {
            Claims claims=JwtHelper.getInstance().parseJWT(token);
            return ApiResult.success(claims);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }
    /**
     * 分类列表查询
     */
    @ApiOperation("人员列表查询")
    @GetMapping("/persons/list")
    public ApiResult selectPersonsList() {
        try {
            List<Persons> list = personsMgrService.getPersonsList();
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类列表查询
     */
    @ApiOperation("人员列表查询按名称")
    @GetMapping("/persons/listbyname")
    public ApiResult selectListByName(@RequestParam String person_name ) {
        try {
            List<Persons> list = personsMgrService.getPersonsList(person_name );
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }
    /**
     * 分类列表查询
     */
    @ApiOperation("人员列表查询按ID")
    @GetMapping("/persons/listbyid")
    public ApiResult selectListByID(@RequestParam Integer person_id ) {
        try {
            List<Persons> list = personsMgrService.getPersonsListByID(person_id );
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }
    /**
     * 分类添加
     */
    @ApiOperation("人员添加")
    @PostMapping("/persons/add")
    public ApiResult addPersons(@RequestBody PersonsBo personsBo) {
        log.info("addPersons:{},{},{},{},{},{},{},{},{}", personsBo);
        Assert.notNull(personsBo.getFirstName(), "分类的名字不能为空!");
        try {
            Persons persons = new Persons();
            BeanUtils.copyProperties(personsBo, persons);
            personsMgrService.addOrUpdPersons(persons);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类修改
     */
    @ApiOperation("人员修改")
    @PostMapping("/persons/update")
    public ApiResult updPersons(@RequestBody PersonsBo personsBo) {
        log.info("updPersons:{},{},{},{},{},{},{},{},{}", personsBo);
        Assert.notNull(personsBo.getIncrementId(), "分类的id不能为空!");
        Assert.notNull(personsBo.getPersonId(), "分类的CategoryId不能为空!");
        Assert.notNull(personsBo.getEmail(), "分类的名字不能为空!");
        try {
            Persons persons = new Persons();
            BeanUtils.copyProperties(personsBo, persons);
            personsMgrService.addOrUpdPersons(persons);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类删除
     */
    @ApiOperation("人员删除")
    @PostMapping("/persons/del")
    public ApiResult delPersons(@RequestParam Integer person_id) {
        log.info("delPersons:{},{},{}", person_id);
        try {
            personsMgrService.delPersons(person_id);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

}
