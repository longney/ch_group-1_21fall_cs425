package com.dvo.controller;


import com.dvo.common.ApiResult;
import com.dvo.domain.DocDetails;
import com.dvo.service.DocDetailsMgrService;
import com.dvo.utils.Audience;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统控制器
 */
@Api("文档管理系统控制器")
@Slf4j
@RestController
public class DocDetailsMgrController {
    @Resource
    private Audience audience;

    @Resource
    private DocDetailsMgrService docdetailsMgrService;



    /**
     * 分类列表查询
     */
    @ApiOperation("文档详情列表查询")
    @GetMapping("/docdetails/detaillist")
    public ApiResult selectDocDetailsList() {
        try {
            List<DocDetails> list = docdetailsMgrService.getDocDetailsList();
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类列表查询
     */
    @ApiOperation("文档详情列表查询")
    @GetMapping("/docdetails/list")
    public ApiResult selectDocDetailList() {
        try {
            List<DocDetails> list = docdetailsMgrService.getDocumentsList();
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类列表查询
     */
    @ApiOperation("文档详情列表查询按名称")
    @GetMapping("/docdetails/detaillistbyid")
    public ApiResult selectDocDetailsListByID(@RequestParam Integer documents_id) {
        try {
            List<DocDetails> list = docdetailsMgrService.getDocumentsList(documents_id);
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }
    /**
     * 分类列表查询
     */
    @ApiOperation("文档详情列表查询按名称")
    @GetMapping("/docdetails/detaillistbyattvalue")
    public ApiResult selectDocDetailsListByAttValue(@RequestParam String Search_Value) {
        try {
            List<DocDetails> list = docdetailsMgrService.getDocSearchList(Search_Value.toUpperCase());
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }
}
