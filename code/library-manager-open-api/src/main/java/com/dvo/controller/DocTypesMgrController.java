package com.dvo.controller;

import com.dvo.bo.DocTypesBo;
import com.dvo.common.ApiResult;
import com.dvo.domain.DocTypes;
import com.dvo.service.DocTypesMgrService;
import com.dvo.utils.Audience;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统控制器
 */
@Api("文档管理系统控制器")
@Slf4j
@RestController
public class DocTypesMgrController {
    @Resource
    private Audience audience;

    @Resource
    private DocTypesMgrService doctypesMgrService;



    /**
     * 分类列表查询
     */
    @ApiOperation("文档类型列表查询")
    @GetMapping("/doctypes/list")
    public ApiResult selectDocTypesList() {
        try {
            List<DocTypes> list = doctypesMgrService.getDocTypesList();
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类列表查询
     */
    @ApiOperation("文档类型列表查询按名称")
    @GetMapping("/doctypes/listbyname")
    public ApiResult selectDocTypesListByName(@RequestParam String doctypes_name) {
        try {
            List<DocTypes> list = doctypesMgrService.getDocTypesList(doctypes_name);
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类添加
     */
    @ApiOperation("文档类型添加")
    @PostMapping("/doctypes/add")
    public ApiResult addDocTypes(@RequestBody DocTypesBo doctypesBo) {
        log.info("addPersons:{},{},{},{}", doctypesBo);
        //Assert.notNull(doctypesBo.getDocTypeName(), "分类的名字不能为空!");
        try {
            DocTypes doctypes = new DocTypes();
            BeanUtils.copyProperties(doctypesBo, doctypes);
            doctypesMgrService.addOrUpdDocTypes(doctypes);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类修改
     */
    @ApiOperation("文档类型修改")
    @PostMapping("/doctypes/update")
    public ApiResult updDocTypes(@RequestBody DocTypesBo doctypesBo) {
        log.info("updPersons:{},{},{},{}", doctypesBo);
        //Assert.notNull(doctypesBo.getIncrementId(), "分类的id不能为空!");
        //Assert.notNull(doctypesBo.getDocTypeId(), "分类的CategoryId不能为空!");
        //Assert.notNull(doctypesBo.getDocTypeName(), "分类的名字不能为空!");
        try {
            DocTypes docTypes = new DocTypes();
            BeanUtils.copyProperties(doctypesBo, docTypes);
            doctypesMgrService.addOrUpdDocTypes(docTypes);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类删除
     */
    @ApiOperation("文档类型删除")
    @PostMapping("/doctypes/del")
    public ApiResult delDocTypes(@RequestParam Integer incrementId) {
        log.info("delDocTypes:{},{},{},{}", incrementId);
        try {
            doctypesMgrService.delDocTypes(incrementId);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

}
