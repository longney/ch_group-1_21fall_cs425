package com.dvo.controller;

import com.dvo.bo.BorrowBo;
import com.dvo.common.ApiResult;
import com.dvo.domain.Borrow;
import com.dvo.service.BorrowMgrService;
import com.dvo.utils.Audience;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统控制器
 */
@Api("文档管理系统控制器")
@Slf4j
@RestController
public class BorrowMgrController {
    @Resource
    private Audience audience;

    @Resource
    private BorrowMgrService borrowMgrService;

    /**
     * 分类列表查询
     */
    @ApiOperation("借阅查询按人员ID")
    @GetMapping("/borrow/listbypersonid")
    public ApiResult selectBorrowListByName(@RequestParam Integer Personid) {
        try {
            List<Borrow> list = borrowMgrService.getBorrowList(Personid);
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类添加
     */
    @ApiOperation("借阅添加")
    @PostMapping("/borrow/add")
    public ApiResult addBorrow(@RequestBody BorrowBo borrowBo) {
        try {
            Borrow category = new Borrow();
            BeanUtils.copyProperties(borrowBo, category);
            borrowMgrService.addOrUpdBorrow(category);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类修改
     */
    @ApiOperation("分类修改")
    @PostMapping("/borrow/update")
    public ApiResult updBorrow(@RequestBody BorrowBo borrowBo) {

        try {
            Borrow category = new Borrow();
            BeanUtils.copyProperties(borrowBo, category);
            borrowMgrService.addOrUpdBorrow(category);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }


}
