package com.dvo.controller;

import com.dvo.bo.HasRBo;
import com.dvo.common.ApiResult;
import com.dvo.domain.HasR;
import com.dvo.service.HasRMgrService;
import com.dvo.utils.Audience;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统控制器
 */
@Api("文档管理系统控制器")
@Slf4j
@RestController
public class HasRMgrController {
    @Resource
    private Audience audience;

    @Resource
    private HasRMgrService hasrMgrService;



    /**
     * 分类列表查询
     */
    @ApiOperation("用户权限列表查询")
    @GetMapping("/HasR/list")
    public ApiResult selectHasRList() {
        try {
            List<HasR> list = hasrMgrService.getHasRList();
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类列表查询
     */
    @ApiOperation("用户权限列表查询按用户编号")
    @GetMapping("/HasR/listbypersonid")
    public ApiResult selectHasRListByPersonID(@RequestParam Integer person_id) {
        try {
            List<HasR> list = hasrMgrService.getHasRList(person_id);
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类添加
     */
    @ApiOperation("用户权限添加")
    @PostMapping("/HasR/add")
    public ApiResult addHasR(@RequestBody HasRBo categoryBo) {

        try {
            HasR category = new HasR();
            BeanUtils.copyProperties(categoryBo, category);
            hasrMgrService.addHasR(category);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }


    /**
     * 分类删除
     */
    @ApiOperation("用户权限删除")
    @PostMapping("/HasR/del")
    public ApiResult delHasR(@RequestParam Integer incrementId) {
        //log.info("delCategory:{},{},{}", incrementId);
        try {
            hasrMgrService.delHasR(incrementId);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

}
