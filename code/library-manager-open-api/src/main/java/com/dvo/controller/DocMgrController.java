package com.dvo.controller;

import com.dvo.bo.CategoryBo;
import com.dvo.common.ApiResult;
import com.dvo.domain.Category;
import com.dvo.service.DocMgrService;
import com.dvo.utils.AccessToken;
import com.dvo.utils.Audience;
import com.dvo.utils.JwtHelper;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统控制器
 */
@Api("文档管理系统控制器")
@Slf4j
@RestController
public class DocMgrController {
    @Resource
    private Audience audience;

    @Resource
    private DocMgrService docMgrService;



    /**
     * 分类列表查询
     */
    @ApiOperation("分类列表查询")
    @GetMapping("/category/list")
    public ApiResult selectCategoryList() {
        try {
            List<Category> list = docMgrService.getCategoryList();
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类列表查询
     */
    @ApiOperation("分类列表查询按名称")
    @GetMapping("/category/listbyname")
    public ApiResult selectCategoryListByName(@RequestParam String category_name) {
        try {
            List<Category> list = docMgrService.getCategoryList(category_name);
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类添加
     */
    @ApiOperation("分类添加")
    @PostMapping("/category/add")
    public ApiResult addCategory(@RequestBody CategoryBo categoryBo) {
        //log.info("addCategory:{},{},{}", categoryBo);
        //Assert.notNull(categoryBo.getCategoryName(), "分类的名字不能为空!");
        try {
            Category category = new Category();
            BeanUtils.copyProperties(categoryBo, category);
            docMgrService.addOrUpdCategory(category);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类修改
     */
    @ApiOperation("分类修改")
    @PostMapping("/category/update")
    public ApiResult updCategory(@RequestBody CategoryBo categoryBo) {
        log.info("updCategory:{},{},{}", categoryBo);
        Assert.notNull(categoryBo.getIncrementId(), "分类的id不能为空!");
        Assert.notNull(categoryBo.getCategoryId(), "分类的CategoryId不能为空!");
        Assert.notNull(categoryBo.getCategoryName(), "分类的名字不能为空!");
        try {
            Category category = new Category();
            BeanUtils.copyProperties(categoryBo, category);
            docMgrService.addOrUpdCategory(category);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类删除
     */
    @ApiOperation("分类删除")
    @PostMapping("/category/del")
    public ApiResult delCategory(@RequestParam Integer incrementId) {
        log.info("delCategory:{},{},{}", incrementId);
        try {
            docMgrService.delCategory(incrementId);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

}
