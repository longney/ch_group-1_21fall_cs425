package com.dvo.controller;

import com.dvo.bo.HasDBo;
import com.dvo.common.ApiResult;
import com.dvo.domain.HasD;
import com.dvo.service.HasDMgrService;
import com.dvo.utils.Audience;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档管理系统控制器
 */
@Api("文档管理系统控制器")
@Slf4j
@RestController
public class HasDMgrController {
    @Resource
    private Audience audience;

    @Resource
    private HasDMgrService hasdMgrService;



    /**
     * 分类列表查询
     */
    @ApiOperation("文档属性列表查询")
    @GetMapping("/HasD/list")
    public ApiResult selectHasDList() {
        try {
            List<HasD> list = hasdMgrService.getHasDList();
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类列表查询
     */
    @ApiOperation("文档属性列表查询按文档编号")
    @GetMapping("/HasD/listbydocumentid")
    public ApiResult selectHasDListByPersonID(@RequestParam Integer document_id) {
        try {
            List<HasD> list = hasdMgrService.getHasDList(document_id);
            return ApiResult.success(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

    /**
     * 分类添加
     */
    @ApiOperation("文档属性添加")
    @PostMapping("/HasD/add")
    public ApiResult addHasD(@RequestBody HasDBo hasdBo) {

        try {
            HasD category = new HasD();
            BeanUtils.copyProperties(hasdBo, category);
            hasdMgrService.addHasD(category);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }


    /**
     * 分类删除
     */
    @ApiOperation("文档属性删除")
    @PostMapping("/HasD/del")
    public ApiResult delHasD(@RequestParam Integer documentId) {
        //log.info("delCategory:{},{},{}", incrementId);
        try {
            hasdMgrService.delHasD(documentId);
            return ApiResult.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ApiResult.error(e.getMessage());
        }
    }

}
