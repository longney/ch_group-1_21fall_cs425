/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     2021/11/17 14:54:52                          */
/*==============================================================*/

SET search_path to "Group1";
DROP TABLE IF EXISTS borrow;
DROP TABLE IF EXISTS subcate;
DROP TABLE IF EXISTS subloc;
DROP TABLE IF EXISTS writes;
DROP TABLE IF EXISTS reference;
DROP TABLE IF EXISTS doc_contents;
DROP TABLE IF EXISTS document_version;
DROP TABLE IF EXISTS storage;
DROP TABLE IF EXISTS document_copies;
DROP TABLE IF EXISTS publish;
DROP TABLE IF EXISTS has_d;
DROP TABLE IF EXISTS documents;
DROP TABLE IF EXISTS doc_types;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS has_r;
DROP TABLE IF EXISTS doc_type_attributes;
DROP TABLE IF EXISTS search_his;
DROP TABLE IF EXISTS search_content;
DROP TABLE IF EXISTS persons;
DROP TABLE IF EXISTS publisher;
DROP TABLE IF EXISTS rights;
DROP TABLE IF EXISTS storage_location;

DROP SCHEMA IF EXISTS "Group1" ;

DROP ROLE IF EXISTS "LMS_U1";

CREATE ROLE "LMS_U1" WITH
  LOGIN
  SUPERUSER
  INHERIT
  CREATEDB
  CREATEROLE
  NOREPLICATION
  PASSWORD '123456';

CREATE SCHEMA IF NOT EXISTS "Group1" AUTHORIZATION "LMS_U1";
SET search_path to "Group1";
/*==============================================================*/
/* Table: Borrow       SET search_path TO "Group1";                                         */
/*==============================================================*/
create table Borrow (
    increment_id SERIAL NOT NULL,
    doc_copy_id integer NOT NULL,
    person_id integer NOT NULL,
    borrow_date timestamp without time zone NOT NULL DEFAULT now(),
    estimated_return_date timestamp without time zone DEFAULT ((now())::timestamp without time zone + '14 days'::interval),
    actual_return_date timestamp without time zone,
   --constraint PK_BORROW primary key (Doc_Copy_ID, Person_ID, Borrow_Date),
   constraint AK_KEY_2_BORROW unique (Increment_ID)
);



/*==============================================================*/
/* Table: Category                                              */
/*==============================================================*/
create table Category (
   Increment_ID         SERIAL not null,
   Category_ID          INT4                 not null,
   Category_Name        VARCHAR(200)         null,
   --constraint PK_CATEGORY primary key (Categoty_ID),
   constraint AK_KEY_2_CATEGORY unique (Increment_ID)
);


/*==============================================================*/
/* Table: Doc_Contents                                          */
/*==============================================================*/
create table Doc_Contents (
   Increment_ID         SERIAL not null,
   Doc_Cont_ID          INT4                 not null,
   Doc_Cont_Title       VARCHAR(200)         null,
   Doc_Cont_KeyWords    VARCHAR(200)         null,
   Doc_Doc_Cont_ID      INT4                 not null,
   --constraint PK_DOC_CONTENTS primary key (Doc_Cont_ID, Doc_Doc_Cont_ID),
   constraint AK_KEY_2_DOC_CONT unique (Increment_ID)
);


/*==============================================================*/
/* Table: Doc_Type_Attributes                                   */
/*==============================================================*/
create table Doc_Type_Attributes (
   Increment_ID         SERIAL not null,
   Doc_Type_Att_ID      INT4                 not null,
   Doc_Type_Att_DESC    VARCHAR(200)         null,
   Display_Order        INT4                 null,
   --constraint PK_DOC_TYPE_ATTRIBUTES primary key (Doc_Type_Att_ID),
   constraint AK_KEY_2_DOC_TYPE unique (Increment_ID)
);


/*==============================================================*/
/* Table: Doc_Types                                             */
/*==============================================================*/
create table Doc_Types (
   Increment_ID         SERIAL not null,
   Doc_Type_ID          INT4                 not null,
   Doc_Type_Name        VARCHAR(200)         null,
   Doc_Type_Code        VARCHAR(200)         null,
   --constraint PK_DOC_TYPES primary key (Doc_Type_ID),
   constraint AK_KEY_2_DOC_TYPE2 unique (Increment_ID)
);



/*==============================================================*/
/* Table: Document_Copies                                       */
/*==============================================================*/
create table Document_Copies (
   Increment_ID         SERIAL not null,
   Doc_Copy_ID          INT4                 not null,
   Doc_Ver_ID           INT4                 null,
   --constraint PK_DOCUMENT_COPIES primary key (Doc_Copy_ID),
   constraint AK_KEY_2_DOCUMENT unique (Increment_ID)
);


/*==============================================================*/
/* Table: Document_Version                                      */
/*==============================================================*/
create table Document_Version (
   Increment_ID         SERIAL not null,
   Doc_Ver_ID           INT4                 not null,
   Doc_Cont_ID          INT4                 null,
   Doc_Doc_Cont_ID      INT4                 null,
   Ver_DESC             VARCHAR(200)         null,
   --constraint PK_DOCUMENT_VERSION primary key (Doc_Ver_ID),
   constraint AK_KEY_2_DOCUMENT2 unique (Increment_ID)
);




/*==============================================================*/
/* Table: Documents                                             */
/*==============================================================*/
create table Documents (
   Increment_ID         SERIAL not null,
   Document_ID          INT4                 not null,
   Category_ID          INT4                 null,
   Doc_Type_ID          INT4                 null,
   --constraint PK_DOCUMENTS primary key (Document_ID),
   constraint AK_KEY_2_DOCUMENT3 unique (Increment_ID)
);




/*==============================================================*/
/* Table: Has_D                                                 */
/*==============================================================*/
create table Has_D (
   Increment_ID         SERIAL not null,
   Doc_Type_Att_ID      INT4                 not null,
   Doc_Document_id          INT4                 not null,
   Doc_Type_Att_VALUE   VARCHAR(200)         null,
   --constraint PK_HAS_D primary key (Doc_Type_Att_ID, Doc_Type_ID),
   constraint AK_KEY_2_HAS_D unique (Increment_ID)
);


/*==============================================================*/
/* Table: Has_R                                                 */
/*==============================================================*/
create table Has_R (
   Increment_ID         SERIAL not null,
   Right_ID             INT4                 not null,
   Person_ID            INT4                 not null,
   --constraint PK_HAS_R primary key (Right_ID, Person_ID),
   constraint AK_KEY_2_HAS_R unique (Increment_ID)
);


/*==============================================================*/
/* Table: Persons                                               */
/*==============================================================*/
create table Persons (
   Increment_ID         SERIAL not null,
   Person_ID            SERIAL not null,
   EMail                VARCHAR(200)         null,
   PassWord             VARCHAR(200)         null,
   First_Name           VARCHAR(200)         null,
   Mid_Name             VARCHAR(200)         null,
   Last_Name            VARCHAR(200)         null,
   Capacity             INT4                 null,
   Period               INT4                 null,
   --constraint PK_PERSONS primary key (Person_ID),
   constraint AK_KEY_2_PERSONS unique (Increment_ID)
);


/*==============================================================*/
/* Table: Publish                                               */
/*==============================================================*/
create table Publish (
   Increment_ID         SERIAL not null,
   Document_ID          INT4                 not null,
   Doc_Ver_ID           INT4                 not null,
   Publisher_ID         INT4                 null,
   Publish_Code         VARCHAR(200)                 null,
   Publish_Date         DATE                 null,
   --constraint PK_PUBLISH primary key (Document_ID, Doc_Ver_ID),
   constraint AK_KEY_2_PUBLISH unique (Increment_ID)
);



/*==============================================================*/
/* Table: Publisher                                             */
/*==============================================================*/
create table Publisher (
   Increment_ID         SERIAL not null,
   Publisher_ID         INT4                 not null,
   Publisher_Name       VARCHAR(200)         null,
   Publisher_Addr       VARCHAR(200)         null,
   Publisher_EMail      VARCHAR(200)         null,
   Publisher_Post       VARCHAR(200)         null,
   --constraint PK_PUBLISHER primary key (Publisher_ID),
   constraint AK_KEY_2_PUBLISHE2 unique (Increment_ID)
);


/*==============================================================*/
/* Table: Reference                                             */
/*==============================================================*/
create table Reference (
   Increment_ID         SERIAL not null,
   Doc_Doc_Cont_ID      INT4                 not null,
   Doc_Doc_Doc_Cont_ID2 INT4                 not null,
   Doc_Cont_ID          INT4                 not null,
   Doc_Doc_Doc_Cont_ID  INT4                 not null,
   --constraint PK_REFERENCE primary key (Doc_Doc_Cont_ID, Doc_Doc_Doc_Cont_ID2, Doc_Cont_ID, Doc_Doc_Doc_Cont_ID),
   constraint AK_KEY_2_REFERENC unique (Increment_ID)
);


/*==============================================================*/
/* Table: Rights                                                */
/*==============================================================*/
create table Rights (
   Increment_ID         SERIAL not null,
   Right_ID             INT4                 not null,
   Right_DESC           VARCHAR(200)         null,
   --constraint PK_RIGHTS primary key (Right_ID),
   constraint AK_KEY_2_RIGHTS unique (Increment_ID)
);


/*==============================================================*/
/* Table: Search_Content                                        */
/*==============================================================*/
create table Search_Content (
   Increment_ID         SERIAL not null,
   Search_Content_ID    INT4                 not null,
   S_Publisher          VARCHAR(200)         null,
   S_DooType            VARCHAR(200)         null,
   S_category           VARCHAR(200)         null,
   S_DooName            VARCHAR(200)         null,
   S_DockeyWord         VARCHAR(200)         null,
   S_DocContTitle       VARCHAR(200)         null,
   S_DocContKeyWord     VARCHAR(200)         null,
   S_Author             VARCHAR(200)         null,
   S_PublishCode        INT4                 null,
   S_PublishDate        DATE                 null,
   --constraint PK_SEARCH_CONTENT primary key (Search_Content_ID),
   constraint AK_KEY_2_SEARCH_C unique (Increment_ID)
);


/*==============================================================*/
/* Table: Search_HIS                                            */
/*==============================================================*/
create table Search_HIS (
   Increment_ID         SERIAL not null,
   Person_ID            INT4                 not null,
   Search_Content_ID    INT4                 not null,
   S_Type               VARCHAR(200)         null,
   S_Value              VARCHAR(200)         null,
   --constraint PK_SEARCH_HIS primary key (Person_ID, Search_Content_ID),
   constraint AK_KEY_2_SEARCH_H unique (Increment_ID)
);





/*==============================================================*/
/* Table: Storage                                               */
/*==============================================================*/
create table Storage (
   Increment_ID         SERIAL not null,
   Doc_Copy_ID          INT4                 not null,
   Location_ID          INT4                 not null,
   --constraint PK_STORAGE primary key (Doc_Copy_ID, Location_ID),
   constraint AK_KEY_2_STORAGE unique (Increment_ID)
);



/*==============================================================*/
/* Table: Storage_Location                                      */
/*==============================================================*/
create table Storage_Location (
   Increment_ID         SERIAL not null,
   Location_ID          INT4                 not null,
   Location_DESC        VARCHAR(200)         null,
   --constraint PK_STORAGE_LOCATION primary key (Location_ID),
   constraint AK_KEY_2_STORAGE2 unique (Increment_ID)
);


/*==============================================================*/
/* Table: SubCate                                               */
/*==============================================================*/
create table SubCate (
   Increment_ID         SERIAL not null,
   Cat_Category_ID      INT4                 not null,
   SubCategory_ID       INT4                 not null,
   --constraint PK_SUBCATE primary key (Cat_Categoty_ID, SubCategary_ID),
   constraint AK_KEY_2_SUBCATE unique (Increment_ID)
);





/*==============================================================*/
/* Table: SubLoc                                                */
/*==============================================================*/
create table SubLoc (
   Increment_ID         SERIAL not null,
   Sto_Location_ID      INT4                 not null,
   Location_ID          INT4                 not null,
   --constraint PK_SUBLOC primary key (Sto_Location_ID, Location_ID),
   constraint AK_KEY_2_SUBLOC unique (Increment_ID)
);



/*==============================================================*/
/* Table: Writes                                                */
/*==============================================================*/
create table Writes (
   Increment_ID         SERIAL not null,
   Doc_Cont_ID          INT4                 not null,
   Doc_Doc_Cont_ID      INT4                 not null,
   Person_ID            INT4                 not null,
   --constraint PK_WRITES primary key (Doc_Cont_ID, Doc_Doc_Cont_ID, Person_ID),
   constraint AK_KEY_2_WRITES unique (Increment_ID)
);
ALTER TABLE IF EXISTS persons
    ALTER COLUMN capacity SET DEFAULT 5;

ALTER TABLE IF EXISTS persons
    ALTER COLUMN period SET DEFAULT 14;


INSERT INTO rights(right_id, right_desc)VALUES ( 1, 'Members');
INSERT INTO rights(right_id, right_desc)VALUES ( 2, 'Librarians');

insert into persons (increment_id, person_id, email, password, first_name, mid_name, last_name, capacity, period)
values (1,1,'42972667@qq.com','MTIzNDU2','Wangshui','','Kang',5,14);
insert into persons (increment_id, person_id, email, password, first_name, mid_name, last_name, capacity, period)
values (2,2,'admin@iit.edu','MTIzNDU2','administrator','','iit',5,14);
insert into persons (increment_id, person_id, email, password, first_name, mid_name, last_name, capacity, period)
values (3,3,'test@iit.edu','MTIzNDU2','test','','iit',5,14);

INSERT INTO has_r(right_id, person_id)VALUES ( 1, 1);
INSERT INTO has_r(right_id, person_id)VALUES ( 1, 2);
INSERT INTO has_r(right_id, person_id)VALUES ( 2, 2);
INSERT INTO has_r(right_id, person_id)VALUES ( 1, 3);
--Newspapers
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (1,1,'Books' ,'ISBN');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (2,2,'Journal Articles' ,'ISSN');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (3,3,'Magazines' ,'ISSN');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (4,4,'Recordings' ,'ISRC');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (5,5,'Scores' ,'ISMN');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (6,6,'Musical Works' ,'ISWC');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (7,7,'Audio-Visual Materials' ,'ISAN');
INSERT INTO doc_types(	increment_id, doc_type_id, doc_type_name, doc_type_code)	VALUES (8,8,'Text Word' ,'ISTC');

INSERT INTO category(increment_id, category_id, category_name)	VALUES (1,1,'Arts & Photography');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (2,2,'Biographies & Memoirs');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (3,3,'Business & Money');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (4,4,'Children''s eBooks');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (5,5,'Comics, Manga & Graphic Novels');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (6,6,'Computers & Technology');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (7,7,'Cookbooks, Food & Wine');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (8,8,'Crafts, Hobbies & Home');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (9,9,'Education & Teaching');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (10,10,'Engineering & Transportation');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (11,11,'Foreign Languages');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (12,12,'Health, Fitness & Dieting');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (13,13,'History');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (14,14,'Humor & Entertainment');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (15,15,'Law');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (16,16,'LGBTQ+ eBooks');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (17,17,'Literature & Fiction');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (18,18,'Medical eBooks');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (19,19,'Mystery, Thriller & Suspense');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (20,20,'Nonfiction');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (21,21,'Parenting & Relationships');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (22,22,'Politics & Social Sciences');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (23,23,'Reference');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (24,24,'Religion & Spirituality');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (25,25,'Romance');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (26,26,'Science & Math');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (27,27,'Science Fiction & Fantasy');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (28,28,'Self-Help');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (29,29,'Sports & Outdoors');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (30,30,'Teen & Young Adult');
INSERT INTO category(increment_id, category_id, category_name)	VALUES (31,31,'Travel');

INSERT INTO has_d(doc_document_id,doc_type_att_id,  doc_type_att_value)	VALUES (2, 1, 'Title1');
INSERT INTO has_d(doc_document_id,doc_type_att_id,  doc_type_att_value)	VALUES (2, 2, 'Name1');
INSERT INTO has_d(doc_document_id,doc_type_att_id,  doc_type_att_value)	VALUES (2, 3, 'Ketword1');