import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'default-passive-events'
// import '@/styles/index.scss'
// import meta from './util/meta.js'

import axios from 'axios'

Vue.use(ElementUI)
// Vue.prototype.token = meta

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

Vue.prototype.$http = axios

axios.defaults.baseURL = 'http://localhost:8766/api'

axios.interceptors.request.use(config => {
  // config.headers.Authorization = 'eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFMyNTYifQ.eyJqdGkiOiIxNWFlYjFiNC05OTlkLTQxMjUtYjhjZS0xOTFjNWJiMDBmYmIiLCJzdWIiOiJqYWNrIiwicm9sZSI6Im1hbmFnZXIiLCJ1bmlxdWVfbmFtZSI6ImphY2siLCJ1aWQiOiIxMCIsImlzcyI6InJlc3RhcGl1c2VyIiwiYXVkIjoiMDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjYiLCJleHAiOjE2Mzc1MDkwNzEsIm5iZiI6MTYzNzUwNTQ3MX0.k9XCUcf374pSZjnIYx7gOYhlmfXvTpxUjr9MuYPSPD8'
  // const localCacheToken = window.sessionStorage.getItem('token')
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // console.log('token in main:' + localCacheToken)
  return config
})
axios.interceptors.response.use(
  res => {
    // 根据res的响应自行判断会话是否有效
    // 若无效则跳转到登录页
    // 参照下面返回401的处理方式
    if (res.data.code !== 200) {
      console.log(res)
      if (res.data.code === 10002) {
      // token expire
        window.sessionStorage.removeItem('token')
        window.currentVue.$message('Token Expire,Login Please！')
        this.$router.push('/index')
      }
      // window.currentVue.$message('Error in response interceptors: ' + res.data.message)
    }
    return res
  },
  err => {
    console.log(err)
    if (err.response === undefined) {
      console.log('network error')
      window.currentVue.$message('Loss connection,please ensure the network and server both work properly!')
      return 0
    } else if (err.response.status !== 200) {
      console.log('http error:' + err)
      window.currentVue.$message('Error in http response!')
      return 0
    } else if (err.response.data.code !== 200) {
      console.log('Error in server internal!' + err)
      return 0
    } else {
      console.log(err)
    }
  }
)
Vue.prototype.$message1 = function (msg) {
  this.$message({ message: msg, duration: 9000 })
}
Vue.prototype.roles = { user: 1, admin: 2 }
Vue.prototype.myroles = ''
Vue.prototype.myid = ''
Vue.prototype.myfullname = ''
Vue.prototype.getroles = function (callback) {
  if (window.sessionStorage.getItem('token') === null) {
    window.currentVue.$message('No Token Found,Login Please!')
  } else {
    this.$http({
      method: 'get',
      url: '/tokenParse?token=' + encodeURIComponent(window.sessionStorage.getItem('token'))
    }).then(res => {
      // console.log(res) // res返回的数据
      // console.log(res.data.data.role)
      if (res === undefined || res === 0) {
        console.log('res undefined' + res)
      } else if (res.data.code === 200) {
        Vue.prototype.myroles = res.data.data.role
        Vue.prototype.myid = res.data.data.uid
        Vue.prototype.myfullname = res.data.data.unique_name
        callback.call()
      } else {
        // console.log(res)
        this.$message('Error in getroles. Code:' + res.data.code + ' Error Msg:' + res.data.message)
      }
    }).catch(err => {
      console.log(err)
      // this.$message('Error :' + err)
    })
  }
}
