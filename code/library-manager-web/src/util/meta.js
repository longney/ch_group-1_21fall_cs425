function setToken (Token) {
  window.sessionStorage.setItem('Token', JSON.stringify(Token))
}

function getToken (context, callback) {
  const permissionStr = window.sessionStorage.getItem('Token')
  if (permissionStr === undefined || permissionStr === null || permissionStr === '{}') {
    context.serverInstance.http.get(context.serverInstance.urls.login.get_permission, '').then(function (res) {
      if (res === undefined) {
        context.$message({ type: 'error', message: '服务端发生异常！', offset: 140, duration: 3000 })
        // callback.call(context, {menus:[],permission:{}});
      } else if (res.status === 200 && res.data.header.code === 0) {
        setToken(res.data.body)
        // callback.call(context, res.data.body);
      } else {
        console.error('获取当前用户权限信息失败！')
        // context.$message({type: "error", message: "获取当前用户权限信息失败！", offset: 140, duration: 3000});
      }
    })
  } else {
    callback.call(context, JSON.parse(permissionStr))
  }
}

export default { set: setToken, get: getToken }
