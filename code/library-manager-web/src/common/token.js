const tokenKey = "token";
//存储
export function setToken(token){
    return sessionStorage.setItem(tokenKey,token);
};
//获取
export function getToken(){
    return sessionStorage.getItem(tokenKey);
};
//清除
export function removeToken(){
    return sessionStorage.removeItem(tokenKey);
};