import axios from 'axios'
import {Message} from 'element-ui'
import {getToken} from '@/common/token'

axios.defaults.baseURL = 'http://localhost:5058/api/'

//请求拦截器
//发送请求前做的什么
axios.interceptors.request.use(config=>{
    //header统一添加token
    config.headers['token'] = getToken();
    return config;
});

//响应拦截器
//对响应数据做的什么
axios.interceptors.response.use(response=>{
    console.log(response);
    let {code,msg} = response.data;
    if(code !=20000){
        Message({
            message: msg || '网络错误！',
            type: 'warning',
            duration:2000
        })
    }
    return response;
},err=>{
    Message({
        message: '服务器不给力！',
        type: 'warning',
        duration:2000
    });
    return Promise.reject(err);
})

export default axios;