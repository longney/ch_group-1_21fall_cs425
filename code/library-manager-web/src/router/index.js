import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/index'
  },
  {
    path: '/home',
    name: 'home',
    meta: { title: 'Home' },
    redirect: '/index',
    component: () => import('../views/home/index.vue'),
    children: [
      {
        path: '/index',
        name: 'index',
        meta: { title: 'Index' },
        component: () => import('../views/home/index/index.vue')
      },
      {
        path: '/search',
        name: 'search',
        meta: { title: 'Search' },
        component: () => import('../views/home/search/index.vue')
      },
      {
        path: '/login',
        name: 'login',
        meta: { title: 'Login' },
        component: () => import('../views/home/login/index.vue')
      },
      {
        path: '/person',
        name: 'person',
        meta: { title: 'Person' },
        component: () => import('../views/home/person/index.vue'),
        children: [
          {
            path: '/person/regist',
            name: 'personregist',
            meta: { title: 'PersonRegist' },
            component: () => import('../views/home/person/personregist.vue')
          },
          {
            path: '/person/personborrowreturn',
            name: 'personborrowreturn',
            meta: { title: 'BorrowReturn' },
            component: () => import('../views/home/person/personborrowreturn.vue')
          },
          {
            path: '/person/personupdate',
            name: 'personupdate',
            meta: { title: 'PersonUpdate' },
            component: () => import('../views/home/person/personupdate.vue')
          }
        ]
      },
      {
        path: '/system',
        name: 'system',
        meta: { title: 'System' },
        component: () => import('../views/home/system/index.vue'),
        children: [
          {
            path: '/system/Persons',
            name: 'Persons',
            meta: { title: 'Persons' },
            component: () => import('../views/home/system/Persons.vue')
          },
          {
            path: '/system/Documents',
            name: 'Documents',
            meta: { title: 'Documents' },
            component: () => import('../views/home/system/Documents.vue')
          },
          {
            path: '/system/Publisher',
            name: 'Publisher',
            meta: { title: 'Publisher' },
            component: () => import('../views/home/system/Publisher.vue')
          },
          {
            path: '/system/Category',
            name: 'Category',
            meta: { title: 'Category' },
            component: () => import('../views/home/system/Category.vue')
          },
          {
            path: '/system/DocTypes',
            name: 'DocTypes',
            meta: { title: 'DocTypes' },
            component: () => import('../views/home/system/DocTypes.vue')
          },
          {
            path: '/system/Storage_Location',
            name: 'Storage_Location',
            meta: { title: 'Storage_Location' },
            component: () => import('../views/home/system/Storage_Location.vue')
          }
        ]
      }

    ]
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  // console.log(to)
  if (to.path === '/login' || to.path === '/index' || to.path === '/search' || to.path === '/person/regist') {
    next()
  } else {
    // console.log('water')
    const token = window.sessionStorage.getItem('token')
    // need to verify token

    // console.log('router session: ' + token)
    if (!token) {
      window.currentVue.$message('Login Please！')
      next('/login')
    } else {
      next()
    }
  }
})
/*
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}
 */
const originalReplace = VueRouter.prototype.replace
VueRouter.prototype.replace = function replace (location) {
  return originalReplace.call(this, location).catch(err => err)
}
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

export default router
